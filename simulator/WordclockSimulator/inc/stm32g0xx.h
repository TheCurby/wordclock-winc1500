#pragma once

#include <cstdint>
typedef struct
{
} I2C_TypeDef;

typedef struct
    {
      }GPIO_TypeDef;

typedef struct
    {
      }SPI_TypeDef;

typedef struct
    {
      }USART_TypeDef;

#define FLASH_BASE            (0x08000000UL)  /*!< FLASH base address */
#define SRAM_BASE             (0x20000000UL)  /*!< SRAM base address */
#define PERIPH_BASE           (0x40000000UL)  /*!< Peripheral base address */
#define IOPORT_BASE           (0x50000000UL)  /*!< IOPORT base address */

#define APB1PERIPH_BASE       PERIPH_BASE
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x00010000UL)
#define AHB1PERIPH_BASE       (PERIPH_BASE + 0x00020000UL)
#define AHB2PERIPH_BASE       (PERIPH_BASE + 0x08000000UL)
#define AHB4PERIPH_BASE       (PERIPH_BASE + 0x18000000UL)
#define APB3PERIPH_BASE       (PERIPH_BASE + 0x20000000UL)
#define AHB3PERIPH_BASE       (PERIPH_BASE + 0x50000000UL)

#define GPIOA_BASE            (IOPORT_BASE + 0x00000000UL)
#define GPIOB_BASE            (IOPORT_BASE + 0x00000400UL)
#define GPIOC_BASE            (IOPORT_BASE + 0x00000800UL)
#define GPIOD_BASE            (IOPORT_BASE + 0x00000C00UL)
#define GPIOE_BASE            (IOPORT_BASE + 0x00001000UL)
#define GPIOF_BASE            (IOPORT_BASE + 0x00001400UL)

#define SPI1_BASE             (APB2PERIPH_BASE + 0x00003000UL)
#define SPI1                ((SPI_TypeDef *) SPI1_BASE)

#define APBPERIPH_BASE        (PERIPH_BASE)
#define I2C2_BASE             (APBPERIPH_BASE + 0x00005800UL)
#define I2C2                ((I2C_TypeDef *) I2C2_BASE)


#define GPIOA               ((GPIO_TypeDef *) GPIOA_BASE)
#define GPIOB               ((GPIO_TypeDef *) GPIOB_BASE)
#define GPIOC               ((GPIO_TypeDef *) GPIOC_BASE)
#define GPIOD               ((GPIO_TypeDef *) GPIOD_BASE)
#define GPIOF               ((GPIO_TypeDef *) GPIOH_BASE)
