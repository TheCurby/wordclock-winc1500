QT       += core gui
LIBS -= -ld2d1

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG -= c++11
#QMAKE_CXXFLAGS += /std:c++latest
CONFIG += static

QMAKE_CXXFLAGS_RELEASE += -O2

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += _SIMULATOR_
DEFINES += USING_WS2812
DEFINES += ENABLE_DCF77
DEFINES += ENABLE_LANGUAGE
DEFINES += ENABLE_SECONDS
DEFINES += ENABLE_NIGHTMODE
DEFINES += ENABLE_LDR
DEFINES += ENABLE_ANIMATIONS
DEFINES += ENABLE_TEMP
DEFINES += ENABLE_TEMP_SYMBOL
DEFINES -= ENABLE_NIGHT_POWEROFF
DEFINES += ENABLE_KEYS
DEFINES += ENABLE_HUMIDITY
DEFINES += ENABLE_HUMIDITY_SYMBOL

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += inc
INCLUDEPATH += ../../firmware/v2.0/generic/inc
INCLUDEPATH += ../../firmware/v2.0/wordclock

SOURCES += \
    main.cpp \
    src/mainwindow.cpp \
    src/mainwindow_slots.cpp \
    src/wordclock/src/driver/hardware.cpp \
    src/wordclock/src/driver/i2c.cpp \
    src/wordclock/src/driver/rtclock.cpp \
    src/wordclock/src/driver/spi.cpp \
    src/wordclock/src/driver/stm32.cpp \
    src/wordclock/src/libs/m24m01.cpp \
    src/wordclock/src/libs/si7020.cpp \
    ../../firmware/v2.0/generic/src/util/dcf77.cpp \
    ../../firmware/v2.0/generic/src/util/key.cpp \
    ../../firmware/v2.0/generic/src/util/timer.cpp \
    ../../firmware/v2.0/wordclock/src/animation.cpp \
    ../../firmware/v2.0/wordclock/src/colors.cpp \
    ../../firmware/v2.0/wordclock/src/container.cpp \
    ../../firmware/v2.0/wordclock/src/sk6812.cpp \
    ../../firmware/v2.0/wordclock/src/ws2812b.cpp \
    ../../firmware/v2.0/wordclock/src/point.cpp \
    ../../firmware/v2.0/wordclock/src/settings.cpp \
    ../../firmware/v2.0/wordclock/src/wordclock.cpp \
    ../../firmware/v2.0/wordclock/src/wordclock_menu.cpp \
    ../../firmware/v2.0/wordclock/src/wordclock_screens.cpp \
    ../../firmware/v2.0/wordclock/src/const.cpp \
    ../../firmware/v2.0/wordclock/src/screen.cpp

HEADERS += \
    inc/stm32g0xx.h \
    inc/mainwindow.h \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
