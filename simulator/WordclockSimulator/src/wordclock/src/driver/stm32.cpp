#include "driver/stm32.hpp"
#include "driver/hardware.hpp"

extern bool bLeftButton;
extern bool bRightButton;
extern bool bMiddleButton;
extern bool bLED;
extern uint8_t u8Light;

void STM32::configPin(GPIO_TypeDef* oGPIO, uint8_t u8Pin, Flags u8Mode, Flags u8Drive, Flags u8Pullup, uint8_t AF) {

}

void STM32::configPinOutput(GPIO_TypeDef* oGPIO, uint8_t u8Pin, bool bVal) {

}

void STM32::configPinInput(GPIO_TypeDef* oGPIO, uint8_t u8Pin, Flags u8Pullup) {

}

void STM32::configPinAnalog(GPIO_TypeDef* oGPIO, uint8_t u8Pin) {

}

void STM32::configPinAF(GPIO_TypeDef* oGPIO, uint8_t u8Pin, uint8_t u8AF) {

}

void STM32::setPin(GPIO_TypeDef* oGPIO, uint8_t u8Pin, bool bVal) {
    if(oGPIO == GPIOB && u8Pin == 8){
        bLED = bVal;
    }
}

void STM32::setPin(GPIO_TypeDef* oGPIO, uint8_t u8Pin) {

}

void STM32::clrPin(GPIO_TypeDef* oGPIO, uint8_t u8Pin) {

}

bool STM32::getPin(GPIO_TypeDef* oGPIO, uint8_t u8Pin) {
    if(oGPIO == KEYLEFT_PORT && u8Pin == KEYLEFT_PIN){
        return bLeftButton;
    }
    if(oGPIO == KEYMIDDLE_PORT && u8Pin == KEYMIDDLE_PIN){
        return bMiddleButton;
    }
    if(oGPIO == KEYRIGHT_PORT && u8Pin == KEYRIGHT_PIN){
        return bRightButton;
    }

    return false;
}

void STM32::setInterrupt(uint8_t u8Flag, uint8_t u8Priority) {

}

uint16_t STM32::getADC() {
    return 255-u8Light;
}
