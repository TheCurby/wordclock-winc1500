#include "driver/rtclock.hpp"

extern RTClock::s_RTC oRTC_l;

void RTClock::disableWP() {

}

void RTClock::set(const s_RTC& oRTC) {
    oRTC_l = oRTC;
}

RTClock::s_RTC RTClock::get() {
    return oRTC_l;
}

uint8_t RTClock::bcd2val(const uint8_t ucVal) {
    return 0;
}

uint8_t RTClock::val2bcd(const uint8_t ucVal) {
    return 0;
}

int32_t RTClock::toTimeStamp(const RTClock::s_RTC& oRTC) {
    return oRTC.u8Hours * 60 + oRTC.u8Minutes;
}
