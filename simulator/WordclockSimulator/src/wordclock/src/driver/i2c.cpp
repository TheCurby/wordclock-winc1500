////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "driver/i2c.hpp"

i2c::i2c(I2C_TypeDef* oI2C_l) {
    oI2C = oI2C_l;
}

bool i2c::init(uint8_t u8Addr, uint8_t u8Size, bool bRead, bool bStop) {
    return true;
}


bool i2c::write(uint8_t u8Val) {
    return true;
}

bool i2c::read(uint8_t* pu8Dest) {
    return true;
}
