////////////////////////////////////////////////////////////////////////////////
// Wordclock V1.1
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "libs/m24m01.hpp"

M24M01::M24M01(i2c &oI2C_l, uint8_t u8Addr) :
		oI2C(oI2C_l) {
	u8I2CAddr = u8Addr;
}

void M24M01::Read(uint32_t usAddr, uint8_t *pucDest, uint16_t usSize) {

}

void M24M01::Write(uint32_t usAddr, uint8_t *pucSrc, uint16_t usSize) {

}

uint8_t M24M01::getAddr(uint32_t u32Addr) {
        return 0;
}
