////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "libs/si7020.hpp"

SI7020::SI7020(i2c& i2c_l) : oI2C(i2c_l) {
}

uint8_t SI7020::readHumidityHold() {
    static uint8_t u8Humidity = 20;
    u8Humidity++;
    if(u8Humidity > 99) u8Humidity = 20;
    return u8Humidity;
}

uint16_t SI7020::readTempHold() {
    static uint16_t u16Tmp = 1800;

    u16Tmp += 20;

    return u16Tmp;
}
