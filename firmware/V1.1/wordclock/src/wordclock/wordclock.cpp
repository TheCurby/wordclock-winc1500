/////////////////////////////////////////////////////////////cleaer();///////////////////
// Wordclock V1.1
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "wordclock/wordclock.hpp"

Wordclock::Wordclock() : oHardware(), oSPI(SPI_EEPROM, 1), oEEPROM(oSPI, SPI_PORT, SPI_PIN), oSettings(oEEPROM), oUSART(
ONEWIRE_USART, 9600), oOneWire(oUSART), oDS18X20(oOneWire) {
	STM32::setPin(GPIOA, 15);
	setPower(true);

	bOn = true;
	bNewData = false;
	s16TempOld = 0;
	u8SubMenu = 0;
	u16ADC = 0;

	tTimeout.StartMs(Const::u16TimeoutTime);
	setMenu(Menu::Clock);

#ifdef USING_DS18B20
	oDS18X20.setResolution(12);
#endif
	oDS18X20.readTemp();

	RTClock::s_RTC oRTCTemp = RTClock::get();
	srand(oRTCTemp.u8Hours * 3600 + oRTCTemp.u8Minutes * 60 + oRTCTemp.u8Seconds);
}

Wordclock::~Wordclock() {
}

bool Wordclock::loop() {
	static uint16_t u16ADCOld = 0;
	static Timer tADC(500);

#ifndef _SIMULATOR_
	IWDG->KR = 0xAAAA;
#endif

	/* LDR */
	u16ADC = STM32::getADC();

	if (tADC.Ready()) {
		uint16_t OffsetMax = oSettings.getU16BrightnessOffsetMax();
		uint16_t OffsetMin = oSettings.getU16BrightnessOffsetMin();

		if (abs((int32_t) u16ADC - (int32_t) u16ADCOld) > (OffsetMax - OffsetMin) / 10) {
			forceRedraw();
		}
		u16ADCOld = u16ADC;
		tADC.Restart();
	}

#ifdef ENABLE_DCF77
	if (oDCF77.receive(STM32::getPin(DCF77_PORT, DCF77_PIN)) && oSettings.isDcf77()) {
		RTClock::set(oDCF77.getRTC(oSettings.getTimeZone()));
		if (CurrentMenu == Menu::Clock) {
			forceRedraw();
		}
	}
#endif

	redraw();

	getKeys();
	if (oKeyTest.getLastStatus()) {
		setMenu(Menu::Test);
	}

	if (oKeyLeft.getLastStatus()) {
		nextMenu();
	}

	if (oKeyMiddle.getLastStatus()) {
		plusminus(true);
	}

	if (oKeyRight.getLastStatus()) {
		plusminus(false);
	}

	if (tTimeout.Ready() && (uint8_t) CurrentMenu < (uint8_t) Menu::MENU_END) {
		oSettings.writeChanges();
		setMenu(Menu::Clock);
	}

	bool bTmp = bNewData;
	bNewData = false;
	return bTmp;
}

void Wordclock::forceRedraw() {
	tDisplay.Stop();
}

void Wordclock::transmit() {
#ifndef _SIMULATOR_
	oDataSend = oScreen.getData();
	oHardware.transmit(&oDataSend, sizeof(Screen::Data));
#endif

	bNewData = true;
}

bool Wordclock::isNight() {
#ifndef ENABLE_NIGHTMODE
	return false;
#endif

	if (!oSettings.isNight() || !tTimeout.Ready()) {
		return false;
	}

	uint16_t u16NightOn = RTClock::toTimeStamp(oSettings.getNightOn());
	uint16_t u16NightOff = RTClock::toTimeStamp(oSettings.getNightOff());
	uint16_t u16Time = RTClock::toTimeStamp(RTClock::get());

	if (u16NightOn > u16NightOff) {
		if (u16Time >= u16NightOn || u16Time < u16NightOff) {
			return true;
		}
	}
	else {
		if (u16Time >= u16NightOn && u16Time < u16NightOff) {
			return true;
		}
	}

	return false;
}

void Wordclock::setPower(bool bVal) {
	if (bVal) {
		STM32::clrPin(POWER_PORT, POWER_PIN);
	}
	else {
		STM32::setPin(POWER_PORT, POWER_PIN);
	}
}

uint8_t Wordclock::getDimmValue() {
#ifndef ENABLE_LDR
	return 0xff;
#endif

	uint16_t Brightness;
	uint8_t Max = oSettings.getU8BrightnessMax();
	uint8_t Min = oSettings.getU8BrightnessMin();
	uint16_t OffsetMax = oSettings.getU16BrightnessOffsetMax();
	uint16_t OffsetMin = oSettings.getU16BrightnessOffsetMin();
	uint16_t ADC = u16ADC;

	if (ADC < OffsetMin) {
		ADC = OffsetMin;
	}
	else if (ADC > OffsetMax) {
		ADC = OffsetMax;
	}

	OffsetMax -= OffsetMin;
	ADC -= OffsetMin;
	ADC = OffsetMax - ADC;

	if (OffsetMax == 0) {
		return 0xff;
	}

	Brightness = Min + (uint16_t) (Max - Min) * ADC / OffsetMax;

	return Brightness;
}

void Wordclock::redraw() {
	static uint8_t u8Seconds = 0;
	RTClock::s_RTC oRTCTemp = RTClock::get();

	if (u8Seconds == 59 && oRTCTemp.u8Seconds == 0 && CurrentMenu == Menu::Clock) {
		forceRedraw();
	}
	else if (u8Seconds != oRTCTemp.u8Seconds && CurrentMenu == Menu::Seconds) {
		forceRedraw();
	}
	u8Seconds = oRTCTemp.u8Seconds;

	if (tDisplay.Ready()) {
		switch (CurrentMenu) {
			case Menu::Hours:
			case Menu::Minutes:
				tDisplay.StartMs(Const::u16BlinkTime / 4);
			break;

			case Menu::Animation:
				tDisplay.StartMs(Const::u32AnimationTime);
			break;

			default:
				tDisplay.StartMs(Const::u16RedrawTime);
			break;
		}

		drawScreen(CurrentMenu);
	}
}

void Wordclock::drawDots(Container& oContainer, uint8_t u8Size, Colors& oColors, DisplayMode eMode) {
	if (eMode == DisplayMode::Word || eMode == DisplayMode::Word2) {
		oColors.setRandom(eMode);
		eMode = DisplayMode::Normal;
	}

	if (u8Size > 0) {
		oContainer.setWord(-1, -1, 1, oColors, eMode);
	}
	if (u8Size > 1) {
		oContainer.setWord(Const::WIDTH, -1, 1, oColors, eMode);
	}
	if (u8Size > 2) {
		oContainer.setWord(Const::WIDTH, Const::HEIGHT, 1, oColors, eMode);
	}
	if (u8Size > 3) {
		oContainer.setWord(-1, Const::HEIGHT, 1, oColors, eMode);
	}
}

void Wordclock::drawScreen(Menu MenuScreen) {
	if (MenuScreen == Menu::Clock || MenuScreen == Menu::Temp || MenuScreen == Menu::Seconds) {
		if (isNight()) {
#if !defined(ENABLE_NIGHT_POWEROFF) || defined(_SIMULATOR_)
			setPower(true);
			oScreen.setPowerDot();
			transmit();
			return;
#endif
			setPower(false);
			return;
		}
		else if (!bOn) {
#ifdef ENABLE_POWER_OFF
			setPower(false);
			return;
#endif
			setPower(true);
			oScreen.setPowerDot();
			transmit();
			return;
		}
	}

	oScreen.clrScreen();
	setPower(true);
	oContainerTmp.clear();

	//@formatter:off
	switch (MenuScreen) {
		default:
		case Menu::Clock: drawClock(); break;
		case Menu::Seconds: drawSeconds(); break;
		case Menu::Temp: drawTemp(); break;
		case Menu::DCF77: drawDCF77(); break;
		case Menu::Hours: drawHours(); break;
		case Menu::Minutes: drawMinutes(); break;
		case Menu::Colors: drawColors(); break;
		case Menu::Light: drawLight(); break;
		case Menu::Animation: drawAnimation(); break;
		case Menu::Night: drawNight(); break;
		case Menu::Lang: drawLang(); break;
		case Menu::Test: drawTest(); break;
	}
				//@formatter:on

	transmit();
}

void Wordclock::drawClock(ClockData oData) {
	const ClockConfig& oClock = Const::ClockConfigs[(uint8_t) oSettings.getLanguage()];
	uint8_t u8Hours = oData.oRTC.u8Hours % 12;
	uint8_t u8Minutes = oData.oRTC.u8Minutes;

	//Switching Hours
	if (u8Minutes >= oClock.HourSwitcher) {
		u8Hours++;
		u8Hours %= 12;
	}

	//Draw Hours
	if (oData.bDrawHours) {
		oData.oContainer.setWord(oClock.Hours[u8Hours][0], oClock.Hours[u8Hours][1], oClock.Hours[u8Hours][2], oData.oColors, oData.eMode);
	}

	//Draw Minutes
	if (oData.bDrawMinutes) {
		uint8_t u8Field = u8Minutes / 5;
		for (uint8_t i = 0; i < 3; i++) {
			oData.oContainer.setWord(oClock.Minutes[u8Field][i][0], oClock.Minutes[u8Field][i][1], oClock.Minutes[u8Field][i][2], oData.oColors, oData.eMode);
		}
	}

	if (oData.bDots) {
		drawDots(oData.oContainer, u8Minutes % 5, oData.oColors, oData.eMode);
	}

	if (oData.bItIs) {
		for (uint8_t i = 0; i < 3; i++) {
			oData.oContainer.setWord(oClock.ItIs[i][0], oClock.ItIs[i][1], oClock.ItIs[i][2], oData.oColors, oData.eMode);
		}
	}

	if (oData.bClock) {
		if (oData.oRTC.u8Minutes < 5 && CurrentMenu != Menu::Temp) {
			oData.oContainer.setWord(oClock.ClockWord[0], oClock.ClockWord[1], oClock.ClockWord[2], oData.oColors, oData.eMode);
		}
	}

	if (oData.bAmPm) {
		u8Hours = oData.oRTC.u8Hours;

		/*if (u8Minutes >= oClock.HourSwitcher) {
		 u8Hours++;
		 u8Hours %= 24;
		 }*/

		if (u8Hours % 24 < 12) {
			oData.oContainer.setWord(oClock.AM[0], oClock.AM[1], oClock.AM[2], oData.oColors, oData.eMode);
		}
		else {
			oData.oContainer.setWord(oClock.PM[0], oClock.PM[1], oClock.PM[2], oData.oColors, oData.eMode);
		}
	}

	if (oClock.excp) {
		oClock.excp((void*) &oData);
	}
}
