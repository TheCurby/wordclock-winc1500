////////////////////////////////////////////////////////////////////////////////
// Wordclock V1.1
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "wordclock/animation.hpp"

static void drawHLine(Container& oContainer, uint8_t x) {
	Point oPoint(x, 0);
	oPoint.oColors.setWhiteOnly(0x80);

	for (uint8_t i = 0; i < Const::HEIGHT; i++) {
		oPoint.u8Y = i;
		oContainer.add(oPoint);
	}
}

static void drawVLine(Container& oContainer, uint8_t y) {
	Point oPoint(0, y);
	oPoint.oColors.setWhiteOnly(0x80);

	for (uint8_t i = 0; i < Const::WIDTH; i++) {
		oPoint.u8X = i;
		oContainer.add(oPoint);
	}
}

Animation::Animation() {
	bRunning = false;
	u8Dir = 0;
	u8DimmValue = 0xff;
	s16AnimationStep = 0;
	oContainerOutput = &oContainerDraw;
	CurrentAnimation = AnimationType::None;
}

Animation::Animation(Container& oContainerOld_l, Container& oContainerNew_l, AnimationType CurrentAnimation_l, Colors& oColors_l, DisplayMode eMode_l) {
	if (oContainerNew_l.size() > 0) {
		bRunning = true;
	}
	else {
		bRunning = false;
	}
	s16AnimationStep = 0;
	u8DimmValue = 0xff;
	CurrentAnimation = CurrentAnimation_l;

	oContainerOld = oContainerOld_l;
	oContainerNew = oContainerNew_l;
	oContainerDraw3.clear();
	oContainerDraw.clear();
	oContainerDraw2.clear();
	oContainerOutput = &oContainerDraw;

	oColors = oColors_l;
	eMode = eMode_l;

	switch (CurrentAnimation) {
		default:
		break;

		case AnimationType::Move:
			u8Dir = rand() % 4;
			oContainerDraw = oContainerNew;

			switch (u8Dir) {
				default:
				case 0:
					oContainerDraw.moveY(-(Const::HEIGHT));
				break;

				case 1:
					oContainerDraw.moveX(Const::WIDTH);
				break;

				case 2:
					oContainerDraw.moveY((Const::HEIGHT));
				break;

				case 3:
					oContainerDraw.moveX(-Const::WIDTH);
				break;
			}

			oContainerDraw.combine(oContainerOld);
		break;

		case AnimationType::Drop:
			oContainerDraw.clear();
			oContainerDraw.combine(oContainerNew);
			oContainerDraw.moveY(-(Const::HEIGHT));
			oContainerDraw.combine(oContainerOld);
		break;

		case AnimationType::Slider:
			u8Dir = rand() % 4;
		break;

		case AnimationType::Hazard:
			oContainerDraw = oContainerNew;
		break;

		case AnimationType::Explode:
			oContainerDraw = oContainerNew;

			for (uint8_t i = 0; i < oContainerDraw.size(); i++) {
				Point& oPointTmp = oContainerDraw.getRef(i);

				if (oPointTmp.u8X < Const::WIDTH / 2) {
					oPointTmp.u8X = rand() % (Const::WIDTH / 2) - Const::WIDTH / 2;
				}
				else {
					oPointTmp.u8X = Const::WIDTH + rand() % (Const::WIDTH / 2);
				}

				if (oPointTmp.u8Y < Const::HEIGHT / 2) {
					oPointTmp.u8Y = rand() % (Const::HEIGHT / 2) - Const::HEIGHT / 2;
				}
				else {
					oPointTmp.u8Y = Const::HEIGHT + rand() % (Const::HEIGHT / 2);
				}
			}
		break;

		case AnimationType::Snake:
			if (rand() % 2 == 0) {
				if (rand() % 2 == 0) {
					Point oPoint(u8SnakeLength * -1, rand() % Const::HEIGHT);

					for (uint8_t i = 0; i < u8SnakeLength; i++) {
						oContainerDraw3.add(oPoint);
						oPoint.u8X += 1;
					}
				}
				else {
					Point oPoint(Const::WIDTH + u8SnakeLength, rand() % Const::HEIGHT);

					for (uint8_t i = 0; i < u8SnakeLength; i++) {
						oContainerDraw3.add(oPoint);
						oPoint.u8X -= 1;
					}
				}
			}
			else {
				if (rand() % 2 == 0) {
					Point oPoint(rand() % Const::WIDTH, u8SnakeLength * -1);

					for (uint8_t i = 0; i < u8SnakeLength; i++) {
						oContainerDraw3.add(oPoint);
						oPoint.u8Y += 1;
					}
				}
				else {
					Point oPoint(rand() % Const::WIDTH, Const::HEIGHT + u8SnakeLength);

					for (uint8_t i = 0; i < u8SnakeLength; i++) {
						oContainerDraw3.add(oPoint);
						oPoint.u8Y -= 1;
					}
				}
			}

			oPointDst = oContainerOld.get(oContainerOld.getRandomWord());
			u8Dir = 0;
			oContainerDraw2.clear();
		break;

		case AnimationType::Falling:
			s16AnimationStep = -1;
			oContainerDraw = oContainerNew;

			for (uint16_t i = 0; i < oContainerDraw.size(); i++) {
				oContainerDraw.getRef(i).u8Y = -1;
			}
		break;
	}
}

void Animation::stop() {
	bRunning = false;
}

bool Animation::running() {
	return (bRunning);
}

Container& Animation::get() {
	return *oContainerOutput;
}

uint8_t Animation::getDimmValue() {
	return u8DimmValue;
}

uint16_t Animation::run() {
	uint16_t u16Time = 0;

	switch (CurrentAnimation) {
		case AnimationType::Move:
			switch (u8Dir) {
				default:
				case 0:
					oContainerDraw.moveY(1);
				break;

				case 1:
					oContainerDraw.moveX(-1);
				break;

				case 2:
					oContainerDraw.moveY(-1);
				break;

				case 3:
					oContainerDraw.moveX(1);
				break;
			}

			oContainerOutput = &oContainerDraw;
			u16Time = 100;

			++s16AnimationStep;
			if (s16AnimationStep >= Const::WIDTH && (u8Dir == 1 || u8Dir == 3)) {
				bRunning = false;
			}
			if (s16AnimationStep >= (Const::HEIGHT) && (u8Dir == 0 || u8Dir == 2)) {
				bRunning = false;
			}
		break;

		case AnimationType::Fade:
			if (oContainerOld.empty() && s16AnimationStep < 110) {
				s16AnimationStep = 110;
			}

			if (s16AnimationStep < 110) {
				oContainerOutput = &oContainerOld;
				if (s16AnimationStep < 100) {
					u8DimmValue = 0xff - ((uint16_t) s16AnimationStep * 0xff / 99);
				}
				else {
					u8DimmValue = 0;
				}
			}
			else {
				oContainerOutput = &oContainerNew;
				u8DimmValue = ((uint16_t) (s16AnimationStep - 110) * 0xff / 99);
			}

			u16Time = 10;

			if (++s16AnimationStep >= 210) {
				bRunning = false;
			}
		break;

		case AnimationType::Plop:
			if (!oContainerOld.empty()) {
				uint8_t u8Rand = rand() % oContainerOld.size();

				oContainerOld.remove(u8Rand);
				oContainerOutput = &oContainerOld;
			}
			else {
				uint8_t u8Rand = rand() % oContainerNew.size();

				Point oPoint = oContainerNew.remove(u8Rand);
				oContainerDraw.add(oPoint);
				oContainerOutput = &oContainerDraw;
			}

			u16Time = 100;
			if (oContainerNew.empty()) {
				bRunning = false;
			}
		break;

		case AnimationType::Drop:
			for (uint8_t i = oContainerDraw.size(); i-- > 0;) {
				Point& oPoint = oContainerDraw.getRef(i);

				if (s16AnimationStep < (Const::HEIGHT)) {
					if (oPoint.u8X >= Const::WIDTH - 1 - s16AnimationStep) {
						oPoint.u8Y++;
					}
				}
				else {
					if (oPoint.u8X < Const::WIDTH - 1 - (s16AnimationStep - (Const::HEIGHT))) {
						oPoint.u8Y++;
					}
				}
			}

			if (++s16AnimationStep >= (Const::HEIGHT) + (Const::WIDTH - 1)) {
				bRunning = false;
			}

			oContainerOutput = &oContainerDraw;
			u16Time = 100;
		break;

		case AnimationType::Matrix: {
			oContainerDraw3.moveY(1);

			if (s16AnimationStep != 0b11111111111 && rand() % 2 == 0) {
				Point oPoint(rand() % Const::WIDTH, 0);
				oContainerDraw3.add(oPoint);
			}

			int16_t s16Pos;
			for (uint16_t i = 0; i < oContainerDraw3.size(); i++) {
				Point& oPointTmp1 = oContainerDraw3.getRef(i);

				s16Pos = oContainerOld.find(oPointTmp1);
				if (s16Pos >= 0) {
					oContainerOld.remove(s16Pos);
				}

				s16Pos = oContainerNew.find(oPointTmp1);
				if (s16Pos >= 0) {
					oContainerOld.add(oContainerNew.getRef(s16Pos));
				}
			}

			oContainerDraw.clear();

			for (uint16_t i = oContainerDraw3.size(); i-- > 0;) {
				Point oPointTmp = oContainerDraw3.get(i);

				for (uint8_t j = 0; j < 7; j++) {
#ifndef _SIMULATOR_
					static const uint8_t MatrixColors[2][7] = { {0x60, 0x30, 0x10, 0x05, 0x03, 0x02, 0x01}, {0x20, 0x10, 0x05, 0}};
#else
					static const uint8_t MatrixColors[2][7] = { {0xff, 0xa0, 0x60, 0x30, 0x20, 0x10, 0x05}, {0x80, 0x50, 0x20, 0}};
#endif
					oPointTmp.oColors.set(0, MatrixColors[0][j], 0, MatrixColors[1][j]);
					oContainerDraw.add(oPointTmp);
					oPointTmp.u8Y--;
				}

				if (oPointTmp.u8Y >= Const::HEIGHT + 7) {
					oContainerDraw3.remove(i);
					s16AnimationStep |= (1 << oPointTmp.u8X);
				}
			}

			oContainerDraw.merge(oContainerOld);
			oContainerOutput = &oContainerDraw;

			u16Time = 80;
			if (s16AnimationStep == 0b11111111111 && oContainerDraw3.empty()) {
				bRunning = false;
			}
		}
		break;

		case AnimationType::Teletype: {
			oContainerDraw.clear();

			for (uint16_t j = 0; j < oContainerOld.size(); j++) {
				Point& oPointTmp = oContainerOld.getRef(j);

				if (oPointTmp.u8Y * Const::WIDTH + oPointTmp.u8X > s16AnimationStep || (oPointTmp.u8Y < 0)) {
					oContainerDraw.add(oPointTmp);
				}
			}

			for (uint16_t j = 0; j < oContainerNew.size(); j++) {
				Point& oPointTmp = oContainerNew.getRef(j);

				if (oPointTmp.u8Y * Const::WIDTH + oPointTmp.u8X < s16AnimationStep) {
					oContainerDraw.add(oPointTmp);
				}
			}

			Point oPoint(s16AnimationStep % Const::WIDTH, s16AnimationStep / Const::WIDTH);
			oPoint.oColors.setWhiteOnly(0x40);
			oContainerDraw.add(oPoint);

			oContainerOutput = &oContainerDraw;
			u16Time = 50;
			if (++s16AnimationStep >= Const::WIDTH * Const::HEIGHT) {
				bRunning = false;
			}
		}
		break;

		case AnimationType::Slider:
			oContainerDraw.clear();

			for (uint16_t j = 0; j < oContainerNew.size(); j++) {
				Point& oPointTmp = oContainerNew.getRef(j);

				if ((oPointTmp.u8X < s16AnimationStep && u8Dir == 0) || (oPointTmp.u8X > (Const::WIDTH - s16AnimationStep - 1) && u8Dir == 1)
					|| (oPointTmp.u8Y < s16AnimationStep && u8Dir == 2) || (oPointTmp.u8Y > (Const::HEIGHT - s16AnimationStep - 1) && u8Dir == 3)) {
					oContainerDraw.add(oPointTmp);
				}
			}

			for (uint16_t j = 0; j < oContainerOld.size(); j++) {
				Point& oPointTmp = oContainerOld.getRef(j);

				if ((oPointTmp.u8X > s16AnimationStep && u8Dir == 0) || (oPointTmp.u8X < (Const::WIDTH - s16AnimationStep - 1) && u8Dir == 1)
					|| (oPointTmp.u8Y > s16AnimationStep && u8Dir == 2) || (oPointTmp.u8Y < (Const::HEIGHT - s16AnimationStep - 1) && u8Dir == 3)) {
					oContainerDraw.add(oPointTmp);
				}
			}

			switch (u8Dir) {
				default:
				case 0:
					drawHLine(oContainerDraw, s16AnimationStep);
				break;
				case 1:
					drawHLine(oContainerDraw, Const::WIDTH - s16AnimationStep - 1);
				break;
				case 2:
					drawVLine(oContainerDraw, s16AnimationStep);
				break;
				case 3:
					drawVLine(oContainerDraw, Const::HEIGHT - s16AnimationStep - 1);
				break;
			}

			oContainerOutput = &oContainerDraw;
			u16Time = 100;
			++s16AnimationStep;
			if (s16AnimationStep >= Const::WIDTH && (u8Dir == 0 || u8Dir == 1)) {
				bRunning = false;
			}
			if (s16AnimationStep >= Const::HEIGHT && (u8Dir == 2 || u8Dir == 3)) {
				bRunning = false;
			}
		break;

		case AnimationType::Hazard: {
			uint8_t u8Mode = rand() % 5;
			Point oPointTmp;

			if (oContainerNew.empty() || oContainerOld.size() >= Const::LEDS) {
				u8Mode = 0;
			}
			else if (oContainerOld.size() < Const::LEDS) {
				u8Mode = 1 + rand() % 4;
			}

			switch (u8Mode) {
				default:
				case 0: {
					uint16_t u16Remove;
					int16_t u16Counter = 0;
					do {
						u16Remove = rand() % oContainerOld.size();
						oPointTmp = oContainerOld.get(u16Remove);

						if (++u16Counter > 400) {
							u16Remove = 0xffff;
							break;
						}
					} while (oContainerDraw.find(oPointTmp) != -1);
					oContainerOld.remove(u16Remove);
				}
				break;

				case 1:
				case 2:
				case 3:
					oPointTmp.oColors = oColors;
					oPointTmp.oColors.setRandom(eMode);
					do {
						oPointTmp.u8X = rand() % Const::WIDTH;
						oPointTmp.u8Y = rand() % Const::HEIGHT;
					} while (oContainerOld.find(oPointTmp) != -1);
					oContainerOld.add(oPointTmp);
				break;

				case 4: {
					int8_t s8Pos;
					oPointTmp = oContainerNew.remove(rand() % oContainerNew.size());
					s8Pos = oContainerOld.find(oPointTmp);
					if (s8Pos > -1) {
						oContainerOld.remove(s8Pos);
					}
					oContainerOld.add(oPointTmp);
				}
				break;
			}

			oContainerOutput = &oContainerOld;
			u16Time = 40;
			if (oContainerNew.empty() && oContainerOld.size() == oContainerDraw.size()) {
				bRunning = false;
			}
		}
		break;

		case AnimationType::Snake: {
			Point oPointTmp;
			Point oPointTmp2;

			if (u8Dir == 2) {
				oContainerDraw3.remove(0);
				oPointTmp = oContainerDraw3.get(u8SnakeLength - 2);
				oPointTmp2 = oContainerDraw3.get(u8SnakeLength - 3);

				if (oPointTmp.u8X > oPointTmp2.u8X) {
					oPointTmp.u8X++;
				}
				else if (oPointTmp.u8X < oPointTmp2.u8X) {
					oPointTmp.u8X--;
				}

				if (oPointTmp.u8Y > oPointTmp2.u8Y) {
					oPointTmp.u8Y++;
				}
				else if (oPointTmp.u8Y < oPointTmp2.u8Y) {
					oPointTmp.u8Y--;
				}
				oContainerDraw3.add(oPointTmp);
				//Set Snake Color
				for (uint8_t i = 0; i < u8SnakeLength; i++) {
					oContainerDraw3.getRef(i).oColors.setWhiteOnly(0x10 + 0x20 * i);
				}

				bRunning = false;
				for (uint8_t i = 0; i < oContainerDraw3.size(); i++) {
					oPointTmp = oContainerDraw3.get(i);
					if (oPointTmp.inLimits()) {
						bRunning = true;
						break;
					}
				}
			}
			else {
				//Move Snake
				oContainerDraw3.remove(0);
				oPointTmp = oContainerDraw3.get(u8SnakeLength - 2);
				switch (rand() % 2) {
					default:
					case 0:
						if (oPointTmp.u8X == oPointDst.u8X) {
							if (oPointTmp.u8Y < oPointDst.u8Y) {
								oPointTmp.u8Y++;
							}
							else if (oPointTmp.u8Y > oPointDst.u8Y) {
								oPointTmp.u8Y--;
							}
						}
						else {
							if (oPointTmp.u8X < oPointDst.u8X) {
								oPointTmp.u8X++;
							}
							else {
								oPointTmp.u8X--;
							}
						}
					break;

					case 1:
						if (oPointTmp.u8Y == oPointDst.u8Y) {
							if (oPointTmp.u8X < oPointDst.u8X) {
								oPointTmp.u8X++;
							}
							else if (oPointTmp.u8X > oPointDst.u8X) {
								oPointTmp.u8X--;
							}
						}
						else {
							if (oPointTmp.u8Y < oPointDst.u8Y) {
								oPointTmp.u8Y++;
							}
							else {
								oPointTmp.u8Y--;
							}
						}
					break;
				}
				oContainerDraw3.add(oPointTmp);

				//Set Snake Color
				for (uint8_t i = 0; i < u8SnakeLength; i++) {
					oContainerDraw3.getRef(i).oColors.setWhiteOnly(0x10 + 0x15 * i);
				}

				//Remove Points
				if (u8Dir == 0) {
					if (oPointTmp == oPointDst) {
						oContainerOld.remove(oContainerOld.find(oPointTmp));

						oPointDst.u8X++;
						if (oContainerOld.find(oPointDst) >= 0) {
							//Nothing
						}
						else {
							oPointDst.u8X -= 2;

							if (oContainerOld.find(oPointDst) >= 0) {
								//Nothing
							}
							else if (!oContainerOld.areaFree()) {
								//oPointDst = oContainerOld.get(rand() % oContainerOld.size());
								oPointDst = oContainerOld.get(oContainerOld.getRandomWord());
								u8Dir = 0;
							}
							else if (!oContainerNew.areaFree()) {
								//oPointDst = oContainerNew.get(rand() % oContainerNew.size());
								oPointDst = oContainerNew.get(oContainerNew.getRandomWord());
								u8Dir = 1;
							}
							else {
								u8Dir = 2;
							}
						}

					}
				}
				else {
					if (oPointTmp == oPointDst) {
						oPointTmp2 = oContainerNew.remove(oContainerNew.find(oPointTmp));
						oContainerDraw2.add(oPointTmp2);

						oPointDst.u8X++;
						if (oContainerNew.find(oPointDst) >= 0) {
							//Nothing
						}
						else {
							oPointDst.u8X -= 2;

							if (oContainerNew.find(oPointDst) >= 0) {
								//Nothing
							}
							else if (!oContainerNew.areaFree()) {
								//oPointDst = oContainerNew.get(rand() % oContainerNew.size());
								oPointDst = oContainerNew.get(oContainerNew.getRandomWord());
								u8Dir = 1;
							}
							else if (!oContainerOld.areaFree()) {
								//oPointDst = oContainerOld.get(rand() % oContainerOld.size());
								oPointDst = oContainerOld.get(oContainerOld.getRandomWord());
								u8Dir = 0;
							}
							else {
								u8Dir = 2;
							}
						}

					}
				}
			}

			oContainerDraw.clear();
			oContainerDraw.combine(oContainerDraw3);
			oContainerDraw.merge(oContainerOld);
			oContainerDraw.merge(oContainerDraw2);
			oContainerOutput = &oContainerDraw;

			u16Time = 50;
		}
		break;

		case AnimationType::Explode: {
			if (!oContainerOld.empty()) {
				for (uint8_t i = oContainerOld.size(); i-- > 0;) {
					Point& oPointTmp = oContainerOld.getRef(i);

					u8Dir = rand() % 2;
					if (u8Dir == 0) {
						if (oPointTmp.u8X < Const::WIDTH / 2) {
							oPointTmp.u8X--;
						}
						else {
							oPointTmp.u8X++;
						}
					}
					else {
						if (oPointTmp.u8Y < Const::HEIGHT / 2) {
							oPointTmp.u8Y--;
						}
						else {
							oPointTmp.u8Y++;
						}
					}

					if (!oPointTmp.inLimits()) {
						oContainerOld.remove(i);
					}
				}
				oContainerOutput = &oContainerOld;
			}
			else {
				bRunning = false;

				for (uint8_t i = 0; i < oContainerDraw.size(); i++) {
					Point& oPointTmp = oContainerDraw.getRef(i);
					Point& oPointTmp2 = oContainerNew.getRef(i);

					u8Dir = rand() % 2;
					if (u8Dir == 0) {
						if (oPointTmp.u8X < oPointTmp2.u8X) {
							oPointTmp.u8X++;
							bRunning = true;
						}
						else if (oPointTmp.u8X > oPointTmp2.u8X) {
							oPointTmp.u8X--;
							bRunning = true;
						}
						else if (oPointTmp.u8Y < oPointTmp2.u8Y) {
							oPointTmp.u8Y++;
							bRunning = true;
						}
						else if (oPointTmp.u8Y > oPointTmp2.u8Y) {
							oPointTmp.u8Y--;
							bRunning = true;
						}
					}
					else {
						if (oPointTmp.u8Y < oPointTmp2.u8Y) {
							oPointTmp.u8Y++;
							bRunning = true;
						}
						else if (oPointTmp.u8Y > oPointTmp2.u8Y) {
							oPointTmp.u8Y--;
							bRunning = true;
						}
						else if (oPointTmp.u8X < oPointTmp2.u8X) {
							oPointTmp.u8X++;
							bRunning = true;
						}
						else if (oPointTmp.u8X > oPointTmp2.u8X) {
							oPointTmp.u8X--;
							bRunning = true;
						}
					}
				}
				oContainerOutput = &oContainerDraw;
			}

			u16Time = 70;
		}
		break;

		case AnimationType::Falling:
			if (!oContainerOld.empty()) {
				if (s16AnimationStep == -1) {
					s16AnimationStep = rand() % oContainerOld.size();
				}

				oContainerOld.getRef(s16AnimationStep).u8Y++;

				if (oContainerOld.getRef(s16AnimationStep).u8Y >= Const::HEIGHT) {
					oContainerOld.remove(s16AnimationStep);
					s16AnimationStep = -1;
				}

				oContainerOutput = &oContainerOld;
			}
			else {
				if (s16AnimationStep == -1) {
					s16AnimationStep = rand() % oContainerDraw.size();
				}

				oContainerDraw.getRef(s16AnimationStep).u8Y++;

				if (oContainerDraw.getRef(s16AnimationStep).u8Y >= oContainerNew.getRef(s16AnimationStep).u8Y) {
					Point p = oContainerDraw.remove(s16AnimationStep);
					oContainerNew.remove(s16AnimationStep);
					oContainerDraw2.add(p);
					s16AnimationStep = -1;
				}

				oContainerDraw3.clear();
				oContainerDraw3.combine(oContainerDraw);
				oContainerDraw3.merge(oContainerDraw2);
				oContainerOutput = &oContainerDraw3;
			}

			if(oContainerNew.empty()){
				bRunning = false;
			}

			u16Time = 30;
		break;

		case AnimationType::None:
		default:
			bRunning = false;
		break;
	}

	return u16Time;
}
