////////////////////////////////////////////////////////////////////////////////
// Wordclock V1.1
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "wordclock/point.hpp"

Point::Point() {
	this->u8X = 0;
	this->u8Y = 0;
}

Point::Point(int16_t u8X_l, int16_t u8Y_l) {
	this->u8X = u8X_l;
	this->u8Y = u8Y_l;
}

bool Point::operator ==(const Point& src) const {
	if (src.u8X == this->u8X && src.u8Y == this->u8Y) {
		return true;
	}
	return false;
}

bool Point::operator !=(const Point& src) const {
	if (src.u8X != this->u8X || src.u8Y != this->u8Y) {
		return true;
	}
	return false;
}

bool Point::inLimits() {
	if (u8X >= 0 && u8X < Const::WIDTH && u8Y >= 0 && u8Y < Const::HEIGHT) {
		return true;
	}

	return false;
}
