////////////////////////////////////////////////////////////////////////////////
// Wordclock V1.1
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "driver/hardware.hpp"
#include "util/dcf77.hpp"
#include "libs/ds18x20.hpp"
#include "util/key.hpp"
#include "wordclock/settings.hpp"
#include "wordclock/screen.hpp"

struct ClockData {
		Container& oContainer;
		RTClock::s_RTC& oRTC;
		Colors& oColors;
		DisplayMode eMode;
		bool bDrawHours;
		bool bDrawMinutes;
		bool bDots;
		bool bItIs;
		bool bClock;
		bool bAmPm;
};

class Wordclock {
	public:
		/* vars */
		bool bOn;
		bool bNewData;
		bool bDir;
		uint8_t u8SubMenu;
		uint16_t u16ADC;
		int16_t s16TempOld;
		RTClock::s_RTC oRTCOld;
		Menu CurrentMenu;
		Screen::Data oDataSend;

		/* objects */
		Hardware oHardware;
		SPI oSPI;
		M95M01 oEEPROM;
		Settings oSettings;
		USART oUSART;
		OneWire oOneWire;
		DS18X20 oDS18X20;
		DCF77 oDCF77;
		Screen oScreen;
		Timer tAnimation;
		Timer tTimeout;
		Timer tDisplay;
		Timer tBlink;
		Timer tTemp;
		Key oKeyLeft;
		Key oKeyMiddle;
		Key oKeyRight;
		Key oKeyTest;
		Key oKeyRightDouble;
		Container oContainerTmp;
		Container oContainerOld;
		Container oContainerDigit[2];
		Animation AnimationClock;
		Animation AnimationMenu;

		/*public*/
		Wordclock();
		~Wordclock();
		bool loop();
		uint8_t getDimmValue();
		void transmit();
		bool isNight();

		/* screen drawing */
		void forceRedraw();
		void redraw();
		void drawScreen(Menu MenuScreen);
		void drawClock(ClockData oData);
		void drawDots(Container& oContainer, uint8_t u8Size, Colors& oColors, DisplayMode eMode = DisplayMode::Normal);
		void setPower(bool bVal);

		/*Screens*/
		void drawClock();
		void drawSeconds();
		void drawTemp();
		void drawDCF77();
		void drawHours();
		void drawMinutes();
		void drawColors();
		void drawLight();
		void drawAnimation();
		void drawNight();
		void drawLang();
		void drawTest();

		/* Menu */
		void getKeys();
		void nextMenu();
		void plusminus(bool bVal);
		void setMenu(Menu Goto);
};
