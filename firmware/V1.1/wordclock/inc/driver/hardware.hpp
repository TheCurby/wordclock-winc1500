////////////////////////////////////////////////////////////////////////////////
// Wordclock V1.1
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "driver/stm32.hpp"
#include "driver/clocks.hpp"
#include "util/timer.hpp"

#define SPI_EEPROM SPI1
#define SPI_PORT GPIOA
#define SPI_PIN 5
#define ONEWIRE_USART USART1
#define DCF77_PORT GPIOA
#define DCF77_PIN 8
#define POWER_PORT GPIOB
#define POWER_PIN 9
#define KEYLEFT_PORT GPIOB
#define KEYLEFT_PIN 5
#define KEYMIDDLE_PORT GPIOB
#define KEYMIDDLE_PIN 4
#define KEYRIGHT_PORT GPIOB
#define KEYRIGHT_PIN 3

class Hardware {
	public:
		Hardware();
		void transmit(void* pData, uint32_t u32Size);
};
