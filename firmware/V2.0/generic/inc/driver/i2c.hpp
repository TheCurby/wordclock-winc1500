////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "driver/stm32.hpp"
#include "driver/clocks.hpp"
#include "util/timer.hpp"

class i2c {
	public:
		i2c(I2C_TypeDef* oI2C_l);
		bool init(uint8_t u8Addr, uint8_t u8Size, bool bRead, bool bStop);
		bool write(uint8_t u8Val);
		bool read(uint8_t* pu8Dest);
		bool finished();
		bool loopFinished();

	private:
		I2C_TypeDef* oI2C;
};
