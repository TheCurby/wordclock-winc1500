////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "driver/stm32.hpp"
#include "driver/clocks.hpp"
#include "util/timer.hpp"

#define SPI_WINC1500 SPI1
#define DCF77_PORT GPIOC
#define DCF77_PIN 8
#define POWER_PORT GPIOB
#define POWER_PIN 12
#define KEYLEFT_PORT GPIOA
#define KEYLEFT_PIN 22
#define KEYMIDDLE_PORT GPIOA
#define KEYMIDDLE_PIN 21
#define KEYRIGHT_PORT GPIOA
#define KEYRIGHT_PIN 20

class Hardware {
	public:
		Hardware();
		void transmit(void* pData, uint32_t u32Size);
};
