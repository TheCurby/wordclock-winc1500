////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "libs/si7020.hpp"

SI7020::SI7020(i2c& i2c_l) : oI2C(i2c_l) {
}

uint8_t SI7020::readHumidityHold() {
	union {
			uint8_t u8[2];
			uint16_t u16;
	} uTemp;

	if (!oI2C.init(u8I2CAddr, 1, false, false)) {
		return 0;
	}

	if (!oI2C.write(MEASURE_HUM_HOLD)) {
		return 0;
	}

	if (!oI2C.loopFinished()) {
		return 0;
	}

	if (!oI2C.init(u8I2CAddr, 2, true, true)) {
		return 0;
	}

	if (!oI2C.read(&uTemp.u8[0])) {
		return 0;
	}

	if (!oI2C.read(&uTemp.u8[1])) {
		return 0;
	}

	if (!oI2C.loopFinished()) {
		return 0;
	}

	return 125 * (uint32_t) uTemp.u16 / 0xffff - 6;
}

uint16_t SI7020::readTempHold() {
	union {
			uint8_t u8[2];
			uint16_t u16;
	} uTemp;

	if (!oI2C.init(u8I2CAddr, 1, false, false)) {
		return 0;
	}

	if (!oI2C.write(MEASURE_TEMP_HOLD)) {
		return 0;
	}

	if (!oI2C.loopFinished()) {
		return 0;
	}

	if (!oI2C.init(u8I2CAddr, 2, true, true)) {
		return 0;
	}

	if (!oI2C.read(&uTemp.u8[0])) {
		return 0;
	}

	if (!oI2C.read(&uTemp.u8[1])) {
		return 0;
	}

	if (!oI2C.loopFinished()) {
		return 0;
	}

	return 17572 * (uint32_t) uTemp.u16 / 0xffff - 4685;
}
