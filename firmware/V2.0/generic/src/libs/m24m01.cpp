////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "libs/m24m01.hpp"

#define WRITE false
#define READ true

M24M01::M24M01(i2c& oI2C_l, uint8_t u8Addr) : oI2C(oI2C_l) {
	u8I2CAddr = u8Addr;
}

void M24M01::Read(uint32_t usAddr, uint8_t* pucDest, uint16_t usSize) {
	tTimeout.StartMs(500);
	uint8_t u8Data[2] = {(uint8_t) ((usAddr >> 8) & 0xff), (uint8_t) (usAddr & 0xff)};
	while (!oI2C.init(getAddr(usAddr), 2, WRITE, false)) {
		if (tTimeout.Ready()) {
			return;
		}
	}
	if (!oI2C.write(u8Data[0])) {
		return;
	}

	if (!oI2C.write(u8Data[1])) {
		return;
	}

	if (!oI2C.loopFinished()) {
		return;
	}

	if (!oI2C.init(getAddr(usAddr), usSize, READ, true)) {
		return;
	}

	while (usSize-- > 0) {
		if (!oI2C.read(pucDest++)) {
			return;
		}
	}

	if (!oI2C.loopFinished()) {
		return;
	}
}

void M24M01::Write(uint32_t usAddr, uint8_t* pucSrc, uint16_t usSize) {
	uint16_t u16TmpSize;
	uint8_t u8Cycles = ((usAddr + usSize) / u16PageSize) - (usAddr / u16PageSize) + 1;

	while (u8Cycles-- > 0) {
		if (u8Cycles != 0) {
			u16TmpSize = ((usAddr + u16PageSize) / u16PageSize) * u16PageSize - usAddr;
		}
		else {
			u16TmpSize = usSize;
		}

		tTimeout.StartMs(500);
		uint8_t u8Data[2] = {(uint8_t) ((usAddr >> 8) & 0xff), (uint8_t) (usAddr & 0xff)};
		while (!oI2C.init(getAddr(usAddr), 2 + u16TmpSize, WRITE, true)) {
			if (tTimeout.Ready()) {
				return;
			}
		}

		if (!oI2C.write(u8Data[0])) {
			return;
		}

		if (!oI2C.write(u8Data[1])) {
			return;
		}

		while (u16TmpSize-- > 0) {
			if (!oI2C.write(*pucSrc++)) {
				return;
			}
		}

		if (!oI2C.loopFinished()) {
			return;
		}

		usSize -= u16TmpSize;
		usAddr += u16TmpSize;
	}
}

uint8_t M24M01::getAddr(uint32_t u32Addr) {
	uint8_t u8I2CAddr_l = u8I2CAddr & 0b11111100;
	if (u32Addr & (1 << 16)) {
		u8I2CAddr_l |= 0x02;
	}

	return u8I2CAddr_l;
}

