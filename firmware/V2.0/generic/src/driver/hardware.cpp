////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "driver/hardware.hpp"

Hardware::Hardware() {
	RCC->PLLCFGR &= ~RCC_PLLCFGR_PLLM_Msk;
	RCC->PLLCFGR = 2 << RCC_PLLCFGR_PLLM_Pos;
	RCC->CR |= RCC_CR_PLLON;
	while(!(RCC->CR & RCC_CR_PLLRDY));
	RCC->CFGR |= 0x02 << RCC_CFGR_SW_Pos;
	while(((RCC->CFGR & RCC_CFGR_SWS_Msk) >> RCC_CFGR_SWS_Pos) != 0x02);
	RCC->CCIPR |= 0b10 << RCC_CCIPR_I2C2SEL_Pos;

	PWR->CR1 |= PWR_CR1_DBP;						//Access to RTC and Backup registers enabled
	//PWR->CR1 |= 0b01ul << PWR_CR1_VOS_Pos;
	if (!(RCC->BDCR & RCC_BDCR_LSERDY)) {
		RCC->BDCR |= RCC_BDCR_LSEON;
		while (!(RCC->BDCR & RCC_BDCR_LSERDY));
	}
	RCC->BDCR |= 0b01ul << RCC_BDCR_RTCSEL_Pos;

	/*setting clocks*/
	RCC->IOPENR |= RCC_IOPENR_GPIOAEN;
	RCC->IOPENR |= RCC_IOPENR_GPIOBEN;
	RCC->IOPENR |= RCC_IOPENR_GPIOCEN;
	RCC->IOPENR |= RCC_IOPENR_GPIODEN;
	RCC->IOPENR |= RCC_IOPENR_GPIOFEN;
	RCC->BDCR |= RCC_BDCR_RTCEN;
	RCC->APBENR2 |= RCC_APBENR2_SPI1EN;
	RCC->APBENR1 |= RCC_APBENR1_TIM6EN;
	RCC->APBENR2 |= RCC_APBENR2_TIM15EN;
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	RCC->APBENR2 |= RCC_APBENR2_ADCEN;
	RCC->APBENR1 |= RCC_APBENR1_I2C2EN;

	/* GPIOs */
	STM32::configPinOutput(GPIOA, 0);		//..
	STM32::configPinAnalog(GPIOA, 1);		//LDR
	STM32::configPin(GPIOA, 2, STM32::Flags::ALTERNATE, STM32::Flags::OPENDRAIN, STM32::Flags::NOPULL, 5); 		//SK6812 Timer
	STM32::configPinInput(GPIOA, 3);  	  	//MTO1
	STM32::configPinInput(GPIOA, 4); 	   	//MTO1
	STM32::configPinInput(GPIOA, 5);   	 	//MTO3
	STM32::configPin(GPIOA, 6, STM32::Flags::ALTERNATE, STM32::Flags::OPENDRAIN, STM32::Flags::NOPULL, 8);    	//SDA I²C2
	STM32::configPin(GPIOA, 7, STM32::Flags::ALTERNATE, STM32::Flags::OPENDRAIN, STM32::Flags::NOPULL, 8);    	//SCL I²C2
	STM32::configPinOutput(GPIOA, 8);		//..
	STM32::configPinOutput(GPIOA, 9);		//..
	STM32::configPinOutput(GPIOA, 10);		//..
	STM32::configPinOutput(GPIOA, 12);		//..
	STM32::configPinOutput(GPIOA, 13);		//..
	//STM32::configPin(GPIOA, 13, STM32::Flags::OUTPUT, STM32::Flags::PUSHPULL, STM32::Flags::NOPULL, 0);
	//STM32::configPin(GPIOA, 14, STM32::Flags::OUTPUT, STM32::Flags::PUSHPULL, STM32::Flags::NOPULL, 0);
	STM32::configPinOutput(GPIOA, 15);		//..

	STM32::configPinOutput(GPIOB, 0);       //..
	STM32::configPinOutput(GPIOB, 1);		//..
	STM32::configPinOutput(GPIOB, 2);		//..
	STM32::configPinAF(GPIOB, 3, 0);    	//SPI1 SCK
	STM32::configPinAF(GPIOB, 4, 0);    	//SPI1 MISO
	STM32::configPinAF(GPIOB, 5, 0);    	//SPI1 MOSI
	STM32::configPinInput(GPIOB, 6, STM32::Flags::PULLUP); //IRQN WINC1500
	STM32::configPinOutput(GPIOB, 7);		//WAKE WINC1500
	STM32::configPinOutput(GPIOB, 8);		//..
	STM32::configPinOutput(GPIOB, 9);		//..
	STM32::configPinOutput(GPIOB, 10);		//..
	STM32::configPin(GPIOB, 11, STM32::Flags::OUTPUT, STM32::Flags::OPENDRAIN, STM32::Flags::NOPULL, 0); //P_LED
	STM32::configPinInput(GPIOB, 12, STM32::Flags::PULLUP);		//Switch Bootloader
	STM32::configPinOutput(GPIOB, 13);		//..
	STM32::configPinOutput(GPIOB, 14);		//..
	STM32::configPinOutput(GPIOB, 15);		//..

	STM32::configPinOutput(GPIOC, 0);		//..
	STM32::configPinOutput(GPIOC, 1);		//..
	STM32::configPinOutput(GPIOC, 2);		//..
	STM32::configPinOutput(GPIOC, 3);		//..
	STM32::configPinOutput(GPIOC, 4);		//..
	STM32::configPinOutput(GPIOC, 5);		//..
	STM32::configPinOutput(GPIOC, 6);		//..
	STM32::configPinOutput(GPIOC, 7);		//..
	STM32::configPinInput(GPIOC, 8);    	//DCF77
	STM32::configPinOutput(GPIOC, 9);		//..
	STM32::configPinOutput(GPIOC, 10);		//..
	STM32::configPinOutput(GPIOC, 11);		//..
	STM32::configPinOutput(GPIOC, 12);		//..
	STM32::configPinOutput(GPIOC, 13);		//..
	//STM32::configPin(GPIOC, 14, STM32::Flags::OUTPUT, STM32::Flags::PUSHPULL, STM32::Flags::NOPULL, 0);
	//STM32::configPin(GPIOC, 15, STM32::Flags::OUTPUT, STM32::Flags::PUSHPULL, STM32::Flags::NOPULL, 0);

	STM32::configPinOutput(GPIOD, 0, false);//RESET WINC1500
	STM32::configPinOutput(GPIOD, 1);		//..
	STM32::configPinOutput(GPIOD, 2);		//..
	STM32::configPinOutput(GPIOD, 3, false);//EN WINC1500
	STM32::configPinOutput(GPIOD, 4);		//..
	STM32::configPinOutput(GPIOD, 5);		//..
	STM32::configPinOutput(GPIOD, 6, true);	//CS WINC1500
	STM32::configPinOutput(GPIOD, 8);		//..
	STM32::configPinOutput(GPIOD, 9);		//..

	STM32::configPinOutput(GPIOF, 0);		//..
	STM32::configPinOutput(GPIOF, 1);		//..

	/*RTC*/
	RTC->WPR = 0xCA;
	RTC->WPR = 0x53;
	//RTC->CR |= RTC_CR_COE;
	//RTC->OR |= (1 << 1);
	RTC->CALR = (uint32_t) (512 - 19) | RTC_CALR_CALP;

	/* Timer Interrupt */
	TIM6->ARR = 0xffffffff;
	TIM6->CNT = 0xffffffff;
	TIM6->DIER = TIM_DIER_UIE;
	TIM6->PSC = (CLK_TIMER / 1000000ul) - 1;
	TIM6->CR1 = TIM_CR1_CEN;
	Timer::setReference((uint32_t*) &TIM6->CNT);
	STM32::setInterrupt(TIM6_IRQn, 1);

	/* Timer SK6812 */
	DMAMUX1_Channel0->CCR = 43 << DMAMUX_CxCR_DMAREQ_ID_Pos;
	DMA1_Channel1->CCR = DMA_CCR_MINC | (0b00 << 10) | (0b01 << 8) | DMA_CCR_DIR;
	TIM15->CR1 = 0;
	TIM15->DIER = TIM_DIER_UDE;
	TIM15->CCMR1 = (0b110UL << 4) | TIM_CCMR1_OC1PE;
	TIM15->CCER = TIM_CCER_CC1E;
	TIM15->ARR = (CLK_TIMER / CLK_SK6812) - 1;
	TIM15->CCR1 = 0;
	TIM15->BDTR = TIM_BDTR_MOE;
	TIM15->CR1 = TIM_CR1_CEN;

	/*EXTI for WINC1500*/
	EXTI->FTSR1 |= EXTI_FTSR1_FT6;
	EXTI->FPR1 |= EXTI_FPR1_FPIF6;
	EXTI->EXTICR[1] = 1 << EXTI_EXTICR2_EXTI6_Pos;
	STM32::setInterrupt(EXTI4_15_IRQn, 1);

	/* LDR */
	ADC1->CR |= ADC_CR_ADVREGEN;
	Timer t(10);
	while (!t.Ready());
	ADC1->CR |= ADC_CR_ADCAL;
	while (ADC1->CR & ADC_CR_ADCAL);
	ADC1->CHSELR |= ADC_CHSELR_CHSEL1;
	ADC1->CR |= ADC_CR_ADEN;
	while (!(ADC1->ISR & ADC_ISR_ADRDY));
	ADC1->CR |= ADC_CR_ADSTART;

#ifndef DEBUG
	/* Watchdog */
	RCC->CSR |= RCC_CSR_LSION;
	while(!(RCC->CSR & RCC_CSR_LSIRDY));

	IWDG->KR = 0xcccc;
	IWDG->KR = 0x5555;
	IWDG->RLR = 8000 * 3; //3 seconds at 8kHz
	while(IWDG->SR != 0);
	IWDG->KR = 0xAAAA;
#endif
}

void Hardware::transmit(void* pData, uint32_t u32Size) {
	Timer tTimeout_l(500);
	while (!(DMA1->ISR & DMA_ISR_TCIF1) && (DMA1_Channel1->CCR & DMA_CCR_EN)) {
		if (tTimeout_l.Ready()) {
			return;
		}
	}

	DMA1_Channel1->CCR &= ~DMA_CCR_EN;
	TIM15->CR1 &= ~TIM_CR1_CEN;
	TIM15->CNT = 0;
	DMA1->IFCR = DMA_IFCR_CTEIF1;
	DMA1->IFCR = DMA_IFCR_CHTIF1;
	DMA1->IFCR = DMA_IFCR_CTCIF1;
	DMA1->IFCR = DMA_IFCR_CGIF1;
	DMA1_Channel1->CPAR = (uint32_t) &TIM15->CCR1;
	DMA1_Channel1->CMAR = (uint32_t) pData;
	DMA1_Channel1->CNDTR = u32Size;

	DMA1_Channel1->CCR |= DMA_CCR_EN;
	TIM15->CR1 |= TIM_CR1_CEN;
}
