////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "driver/i2c.hpp"

i2c::i2c(I2C_TypeDef* oI2C_l) {
	oI2C = oI2C_l;

	/* 100kHz - calc from Datasheet*/
	oI2C->TIMINGR |= (1 << I2C_TIMINGR_PRESC_Pos);
	oI2C->TIMINGR |= (0x13 << I2C_TIMINGR_SCLL_Pos);
	oI2C->TIMINGR |= (0x0f << I2C_TIMINGR_SCLH_Pos);
	oI2C->TIMINGR |= (0x02 << I2C_TIMINGR_SDADEL_Pos);
	oI2C->TIMINGR |= (0x04 << I2C_TIMINGR_SCLDEL_Pos);

	oI2C->CR1 |= I2C_CR1_PE;
}
bool i2c::init(uint8_t u8Addr, uint8_t u8Size, bool bRead, bool bStop) {
	if (bStop) {
		oI2C->CR2 |= I2C_CR2_STOP;
	}
	oI2C->ICR |= I2C_ICR_NACKCF;    //Clear NACK Flag
	oI2C->CR2 &= ~(I2C_CR2_NBYTES_Msk << I2C_CR2_NBYTES_Pos);    //clear n bytes
	oI2C->CR2 |= u8Size << I2C_CR2_NBYTES_Pos;
	if (bRead) {
		oI2C->CR2 |= I2C_CR2_RD_WRN;    //Reading
	}
	else {
		oI2C->CR2 &= ~I2C_CR2_RD_WRN;    //Reading
	}
	oI2C->CR2 &= ~0x3ff;    //clear slave adress
	oI2C->CR2 |= u8Addr;    //set slave adress
	oI2C->CR2 |= I2C_CR2_START;    //start condition

	while (oI2C->CR2 & I2C_CR2_START);

	if (oI2C->ISR & I2C_ISR_NACKF) {
		return false;
	}

	return true;
}

bool i2c::write(uint8_t u8Val) {
	while (!(oI2C->ISR & I2C_ISR_TXIS));
	oI2C->TXDR = u8Val;

	if (oI2C->ISR & I2C_ISR_NACKF) {
		return false;
	}

	return true;
}

bool i2c::read(uint8_t* pu8Dest) {
	while (!(oI2C->ISR & I2C_ISR_RXNE));
	*pu8Dest = oI2C->TXDR;

	if (oI2C->ISR & I2C_ISR_NACKF) {
		return false;
	}

	return true;
}

bool i2c::finished() {
	return oI2C->ISR & I2C_ISR_TC ? true : false;
}

bool i2c::loopFinished() {
	Timer tTimeout;
	tTimeout.StartMs(500);
	while (!finished()) {
		if (tTimeout.Ready()) {
			return false;
		}
	}

	return true;
}
