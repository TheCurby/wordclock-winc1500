////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "inc/wordclock.hpp"
#include "inc/wifi.hpp"

Wordclock oWordclock;
SPI Wifi::oSPI(SPI_WINC1500, 1);

/* WINC1500 Interrupt */
extern tpfNmBspIsr gpfIsr;
void EXTI4_15_IRQHandler() {
	EXTI->FPR1 |= EXTI_FPR1_FPIF6;

	if (gpfIsr) {
		gpfIsr();
	}
}

/* Timer overflow. TIM6 is clk source for every softwaretimer */
void TIM6_IRQHandler() {
	TIM6->SR &= ~TIM_SR_UIF;

	Timer::incOverflow();
}

/* main */
int main() {
#ifdef ENABLE_WIFI
	Wifi::init();
#endif

	while (true) {
		oWordclock.loop();
#ifdef ENABLE_WIFI
		Wifi::loop();
#endif
	}

	return 0;
}
