/**
 *
 * \file
 *
 * \brief This module contains NMC1000 bus wrapper APIs implementation.
 *
 * Copyright (c) 2016-2021 Microchip Technology Inc. and its subsidiaries.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Subject to your compliance with these terms, you may use Microchip
 * software and any derivatives exclusively with Microchip products.
 * It is your responsibility to comply with third party license terms applicable
 * to your use of third party software (including open source software) that
 * may accompany Microchip software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY,
 * AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE
 * LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL
 * LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE
 * SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE
 * POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT
 * ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY
 * RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
 * THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * \asf_license_stop
 *
 */

#include <stdio.h>
#include "bsp/include/nm_bsp.h"
#include "common/include/nm_common.h"
#include "bus_wrapper/include/nm_bus_wrapper.h"
#include "conf_winc.h"
#include "bsp/include/cpp_interface.h"

#define NM_BUS_MAX_TRX_SZ	4096

tstrNmBusCapabilities egstrNmBusCapabilities = {
NM_BUS_MAX_TRX_SZ};

sint8 nm_spi_rw(uint8* pu8Mosi, uint8* pu8Miso, uint16 u16Sz) {
	uint8_t u8Dummy1 = 0;
	uint8_t u8Dummy2 = 0;
	bool bIgnoreMOSI = false;
	bool bIgnoreMISO = false;

	if (!pu8Mosi) {
		pu8Mosi = &u8Dummy1;
		bIgnoreMOSI = true;
	}
	if (!pu8Miso) {
		pu8Miso = &u8Dummy2;
		bIgnoreMISO = true;
	}

	clrPin(CONF_WINC_SPI_CS_PORT, CONF_WINC_SPI_CS_PIN);
	while (u16Sz-- > 0) {
		*pu8Miso = sendSPI(*pu8Mosi);

		if (!bIgnoreMOSI) {
			pu8Mosi++;
		}
		if (!bIgnoreMISO) {
			pu8Miso++;
		}
	}
	setPin(CONF_WINC_SPI_CS_PORT, CONF_WINC_SPI_CS_PIN);

	return M2M_SUCCESS;
}

/*
 *	@fn		nm_bus_ioctl
 *	@brief	send/receive from the bus
 *	@param[IN]	u8Cmd
 *					IOCTL command for the operation
 *	@param[IN]	pvParameter
 *					Arbitrary parameter depending on IOCTL
 *	@return	M2M_SUCCESS in case of success and M2M_ERR_BUS_FAIL in case of failure
 *	@note	For SPI only, it's important to be able to send/receive at the same time
 */
sint8 nm_bus_ioctl(uint8 u8Cmd, void* pvParameter) {
	sint8 s8Ret = 0;
	switch (u8Cmd) {
		case NM_BUS_IOCTL_RW: {
			tstrNmSpiRw* pstrParam = (tstrNmSpiRw*) pvParameter;
			s8Ret = nm_spi_rw(pstrParam->pu8InBuf, pstrParam->pu8OutBuf, pstrParam->u16Sz);
		}
		break;

		default:
			s8Ret = -1;
			M2M_ERR("invalid ioclt cmd\n");
		break;
	}

	return s8Ret;
}

sint8 nm_bus_init(void* pvinit) {
	//Nothing; not used
	return M2M_SUCCESS;
}

sint8 nm_bus_deinit(void) {
	//Nothing; not used
	return M2M_SUCCESS;
}

sint8 nm_bus_reinit(void* config) {
	//Nothing; not used
	return M2M_SUCCESS;
}
