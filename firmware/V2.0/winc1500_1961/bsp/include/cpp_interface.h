/*
 * cpp_interface.h
 *
 *  Created on: Feb 2, 2022
 *      Author: Nils
 */

#ifndef WINC1500_BSP_INCLUDE_CPP_INTERFACE_H_
#define WINC1500_BSP_INCLUDE_CPP_INTERFACE_H_


#include <stdint.h>
#include "stm32g0xx.h"

#ifdef __cplusplus
extern "C" {
#endif

void clrPin(GPIO_TypeDef* oGPIO, uint8_t u8Pin);
void setPin(GPIO_TypeDef* oGPIO, uint8_t u8Pin);
uint8_t sendSPI(uint8_t u8Val);
void delay_ms(uint16_t ms);
void enableInterrupt(uint8_t u8Enable);

#ifdef __cplusplus
}
#endif


#endif /* WINC1500_BSP_INCLUDE_CPP_INTERFACE_H_ */
