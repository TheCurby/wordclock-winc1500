////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <cstring>
#include <string.h>
#include "libs/m24m01.hpp"
#include "driver/rtclock.hpp"
#include "inc/animation.hpp"

class Settings {
	public:
		Settings(M24M01& oM24M01_l);
		void writeChanges();
		void writeConfig();

		struct s_Data {
				bool bDCF77;
				bool bNight;
				bool bEnableLDR;
				DisplayMode eMode;
				int8_t s8TimeZone;
				uint8_t u8BrightnessMin;
				uint8_t u8BrightnessMax;
				uint16_t u16BrightnessOffsetMin;
				uint16_t u16BrightnessOffsetMax;
				Colors oColors;
				Animation::AnimationType eAnimation;
				Language oLanguage;
				RTClock::s_RTC rtcNightOff;
				RTClock::s_RTC rtcNightOn;

				/*wifi*/
				bool bAccessPoint;
				bool bFetchInternetTime;
				char ssid[Const::MAXLENGTH_SSID + 1];
				char wpakey[Const::MAXLENGTH_WPAKEY + 1];
				char deviceName[Const::MAXLENGTH_DEVICENAME + 1];
				char timeserver[64];
		} oData;

	private:
		M24M01& oM24M01;
		void loadData(s_Data& oData_l);
};
