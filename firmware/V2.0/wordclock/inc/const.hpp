////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <cstdint>

enum DisplayMode : uint8_t {
	Normal,
	Colorful,
	Word,
	Colorful2,
	Word2,
	COUNT_MODE
};

enum Language : uint8_t {
	English,
	Dutch,
	German,
	Italian,
	COUNT_LANG,

	French,
	Spanish
};

enum class Menu : uint8_t {
	DCF77,
	Hours,
	Minutes,
	Colors,
	Light,
	Animation,
	Night,
	Lang,
	MENU_END,

	Clock,
	Temp,
	Humidity,
	Seconds,
	Test
};

struct Image {
		uint8_t u8Size;
		struct {
				uint8_t u8X;
				uint8_t u8Y;
		} u8Points[];
};

struct ClockConfig {
		char MenuChars[3];
		uint8_t HourSwitcher;
		uint8_t Hours[12][3];
		uint8_t Minutes[12][3][3];
		uint8_t ItIs[3][3];
		uint8_t ClockWord[3];
		uint8_t TempSymbol[2];
		uint8_t HumiditySymbol[2];
		uint8_t AM[3];
		uint8_t PM[3];
		void (*excp)(void*);
};

class Const {
	public:
		static constexpr uint16_t WIDTH = 11;
		static constexpr uint16_t HEIGHT = 10;
		static constexpr uint16_t LEDS = (WIDTH * HEIGHT + 4);
		static constexpr uint8_t MAXLENGTH_SSID = 32;
		static constexpr uint8_t MAXLENGTH_WPAKEY = 63;
		static constexpr uint8_t MAXLENGTH_DEVICENAME = 20;
		static constexpr uint8_t MAXLENGTH_TIMESERVER = 50;

		static const ClockConfig ClockConfigs[COUNT_LANG];

		static const Menu MenuMapping[];
		static const uint8_t au8Mapping[LEDS];
		static const uint16_t u16TimeoutTime;
		static const uint16_t u16BlinkTime;
		static const uint16_t u16TestTime;
		static const uint16_t u16RedrawTime;
		static const uint32_t u32AnimationTime;
		static const Image* aoBuchstaben[24];
		static const Image* aoZiffern[10];
		static const Image antenna;
		static const Image error;
		static const Image ok;
		static const Image question;
		static const Image Down;
		static const Image Up;
};
