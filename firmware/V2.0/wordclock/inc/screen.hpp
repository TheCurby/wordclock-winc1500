////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <string.h>
#include "driver/rtclock.hpp"
#include "inc/animation.hpp"
#include "inc/sk6812.hpp"
#include "inc/ws2812b.hpp"

class Screen {
	public:
		struct Data {
#ifdef USING_WS2812
            WS2812B LEDs[Const::LEDS];
#else
			SK6812 LEDs[Const::LEDS];
#endif
			uint8_t u8Void[70];
		};

		Screen();
		void clrScreen();
		void drawContainer(Container& oContainer_l, uint16_t u16Brightness = 0xff);
		void drawFrame(Colors& oColors_l);
		void drawCenter(Colors& oColors);
		void setPowerDot();
		void setLED(int16_t u8X, int16_t u8Y, Colors& oColors);
		void setLED(uint16_t u8N, Colors& oColors);
		Data getData();

	private:
		Data oData;
};
