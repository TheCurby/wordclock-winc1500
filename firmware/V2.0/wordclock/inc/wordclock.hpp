////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "driver/hardware.hpp"
#include "util/dcf77.hpp"
#include "util/key.hpp"
#include "inc/settings.hpp"
#include "inc/screen.hpp"
#include "libs/si7020.hpp"

struct ClockData {
		Container& oContainer;
		RTClock::s_RTC& oRTC;
		Colors& oColors;
		DisplayMode eMode;
		bool bDrawHours;
		bool bDrawMinutes;
		bool bDots;
		bool bItIs;
		bool bClock;
		bool bAmPm;
};

class Wordclock {
	public:
		/* vars */
		bool bOn;
		bool bNewData;
		bool bDir;
		uint8_t u8SubMenu;
		uint8_t u8HumidityOld;
		uint16_t u16ADC;
		uint16_t u16TempOld;
		RTClock::s_RTC oRTCOld;
		Menu CurrentMenu;
		Screen::Data oDataSend;
		struct{
				Timer t;
				uint8_t u8Counter;
				bool bEnable;
		} oIdentify;

		/* objects */
		Hardware oHardware;
		i2c oI2C;
		SI7020 oSI7020;
		M24M01 oEEPROM;
		Settings oSettings;
		DCF77 oDCF77;
		Screen oScreen;
		Timer tAnimation;
		Timer tTimeout;
		Timer tDisplay;
		Timer tBlink;
		Timer tTemp;
		Key oKeyLeft;
		Key oKeyMiddle;
		Key oKeyRight;
		Key oKeyTest;
		Key oKeyRightDouble;
		Container oContainerTmp;
		Container oContainerOld;
		Container oContainerDigit[2];
		Animation AnimationClock;
		Animation AnimationMenu;

		/*public*/
		Wordclock();
		~Wordclock();
		bool loop();
		uint8_t getDimmValue();
		void transmit();
		bool isNight();
		void identify();

		/* screen drawing */
		void forceRedraw();
		void redraw();
		void drawScreen(Menu MenuScreen);
		void drawClock(ClockData oData);
		void drawDots(Container& oContainer, uint8_t u8Size, Colors& oColors, DisplayMode eMode = DisplayMode::Normal);
		void setPower(bool bVal);

		/*Screens*/
		void drawClock();
		void drawSeconds();
		void drawTemp();
		void drawHumidity();
		void drawDCF77();
		void drawHours();
		void drawMinutes();
		void drawColors();
		void drawLight();
		void drawAnimation();
		void drawNight();
		void drawLang();
		void drawTest();

		/* Menu */
		void getKeys();
		void nextMenu();
		void plusminus(bool bVal);
		void setMenu(Menu Goto);
};
