////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "inc/screen.hpp"

Screen::Screen() {
	clrScreen();
	memset(&oData.u8Void, 0, 70);
}

Screen::Data Screen::getData() {
	return oData;
}

void Screen::clrScreen() {
	Colors oColors;

	for (uint16_t i = 0; i < Const::LEDS; i++) {
		setLED(i, oColors);
	}
}

void Screen::drawFrame(Colors& oColors) {
	for (uint8_t i = 0; i < Const::WIDTH; i++) {
		setLED(i, 0, oColors);
	}
	for (uint8_t i = 0; i < Const::WIDTH; i++) {
		setLED(i, Const::HEIGHT - 1, oColors);
	}
	for (uint8_t i = 0; i < (Const::HEIGHT - 2); i++) {
		setLED(Const::WIDTH - 1, 1 + i, oColors);
	}
	for (uint8_t i = 0; i < (Const::HEIGHT - 2); i++) {
		setLED(0, 1 + i, oColors);
	}
}

void Screen::drawCenter(Colors& oColors) {
	for (uint8_t i = 0; i < 4; i++) {
		setLED(3, 3 + i, oColors);
		setLED(4, 3 + i, oColors);
		setLED(5, 3 + i, oColors);
		setLED(6, 3 + i, oColors);
		setLED(7, 3 + i, oColors);
	}
}

void Screen::drawContainer(Container& oContainer_l, uint16_t u16Brightness) {
	for (uint8_t i = 0; i < oContainer_l.size(); i++) {
		Point oPoint = oContainer_l.get(i);

		if (u16Brightness < 0xff) {
			oPoint.oColors.dimm(u16Brightness);
		}

		setLED(oPoint.u8X, oPoint.u8Y, oPoint.oColors);
	}
}

void Screen::setPowerDot() {
	Colors oColors;

	clrScreen();
	oColors.setRedOnly(0x80);
	setLED(110, oColors);
}

void Screen::setLED(uint16_t u8N, Colors& oColors) {
	if (u8N >= Const::LEDS) {
		return;
	}

#ifndef _SIMULATOR_
	oData.LEDs[Const::au8Mapping[u8N]].set(oColors);
#else
	oData.LEDs[u8N].set(oColors);
#endif
}

void Screen::setLED(int16_t u8X, int16_t u8Y, Colors& oColors) {
	if (u8X == -1 && u8Y == -1) {
		setLED(Const::WIDTH * Const::HEIGHT + 0, oColors);
	}
	else if (u8X == Const::WIDTH && u8Y == -1) {
		setLED(Const::WIDTH * Const::HEIGHT + 1, oColors);
	}
	else if (u8X == Const::WIDTH && u8Y == Const::HEIGHT) {
		setLED(Const::WIDTH * Const::HEIGHT + 2, oColors);
	}
	else if (u8X == -1 && u8Y == Const::HEIGHT) {
		setLED(Const::WIDTH * Const::HEIGHT + 3, oColors);
	}
	else {
		if (u8X < 0 || u8Y < 0 || u8X >= Const::WIDTH || u8Y >= Const::HEIGHT) {
			return;
		}

		setLED(u8Y * Const::WIDTH + u8X, oColors);
	}
}
