////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "inc/settings.hpp"

Settings::Settings(M24M01& oM24M01_l) : oM24M01(oM24M01_l) {
	constexpr uint32_t u32Key = 0x7e3ab23b;
	uint32_t u32Key_l = 0;
	oM24M01.Read(M24M01::ut32MemSize - sizeof(uint32_t), (uint8_t*) &u32Key_l, sizeof(uint32_t));

	if (u32Key_l != u32Key) {
		oData.oLanguage = Language::English;
		oData.bDCF77 = false;
		oData.bNight = false;
		oData.bEnableLDR = false;
		oData.s8TimeZone = 0;
		oData.eMode = DisplayMode::Normal;
		oData.eAnimation = Animation::None;
		oData.oColors.setWhiteOnly(0x80);
		oData.u16BrightnessOffsetMax = 0;
		oData.u16BrightnessOffsetMin = 0;
		oData.u8BrightnessMax = 0xff;
		oData.u8BrightnessMin = 0x80;

		/*wifi*/
		oData.bAccessPoint = false;
		oData.bFetchInternetTime = false;
		strcpy(oData.ssid, "wordclock");
		strcpy(oData.wpakey, "");
		strcpy(oData.deviceName, "wordclock");
		strcpy(oData.timeserver, "*.pool.ntp.org");

		writeConfig();
		oM24M01.Write(M24M01::ut32MemSize - sizeof(uint32_t), (uint8_t*) &u32Key, sizeof(uint32_t));
	}
	else {
		loadData(oData);
	}

	/*Last char always \0*/
	oData.ssid[Const::MAXLENGTH_SSID] = '\0';
	oData.wpakey[Const::MAXLENGTH_WPAKEY] = '\0';
	oData.deviceName[Const::MAXLENGTH_DEVICENAME] = '\0';
	oData.timeserver[Const::MAXLENGTH_TIMESERVER] = '\0';

#ifndef ENABLE_LANGUAGE
	oData.oLanguage = DefaultLanguage;
#endif
#ifndef ENABLE_ANIMATIONS
	oData.eAnimation = Animation::None;
#endif
#ifdef USING_WS2812
	oData.oColors.setWhite(0);
#endif
}

void Settings::loadData(s_Data& oData_l) {
	oM24M01.Read(0, (uint8_t*) &oData_l, sizeof(s_Data));

	if (oData.bDCF77 != false && oData.bDCF77 != true) {
		oData.bDCF77 = false;
	}

	if (oData.bNight != false && oData.bNight != true) {
		oData.bNight = false;
	}

	if (oData.bEnableLDR != false && oData.bEnableLDR != true) {
		oData.bEnableLDR = false;
	}

	if (oData.eMode >= DisplayMode::COUNT_MODE) {
		oData.eMode = DisplayMode::Normal;
	}

	if (oData.s8TimeZone > 12 || oData.s8TimeZone < -12) {
		oData.s8TimeZone = 0;
	}

	if (oData.u8BrightnessMax < oData.u8BrightnessMin) {
		oData.u8BrightnessMax = oData.u8BrightnessMin;
	}

	if (oData.oColors.isClear()) {
		oData.oColors.setWhiteOnly(0xff);
	}

	if (oData.eAnimation > Animation::AnimationType::None) {
		oData.eAnimation = Animation::AnimationType::None;
	}

	if (oData.oLanguage >= Language::COUNT_LANG) {
		oData.oLanguage = Language::English;
	}

	if (oData.rtcNightOn.u8Hours > 23) {
		oData.rtcNightOn.u8Hours = 0;
	}
	if (oData.rtcNightOn.u8Minutes > 59) {
		oData.rtcNightOn.u8Hours = 0;
	}

	if (oData.rtcNightOff.u8Hours > 23) {
		oData.rtcNightOff.u8Hours = 0;
	}

	if (oData.rtcNightOff.u8Minutes > 59) {
		oData.rtcNightOff.u8Hours = 0;
	}

	if (oData.bAccessPoint != false && oData.bAccessPoint != true) {
		oData.bAccessPoint = false;
	}

	if (oData.bFetchInternetTime != false && oData.bFetchInternetTime != true) {
		oData.bFetchInternetTime = false;
	}

	if(*oData.ssid == '\0'){
		strcpy(oData.ssid, "wordclock");
	}

	if(*oData.deviceName == '\0'){
		strcpy(oData.deviceName, "wordclock");
	}

	if(*oData.timeserver == '\0'){
		strcpy(oData.timeserver, "*.pool.ntp.org");
	}
}

void Settings::writeChanges() {
	s_Data oData_l;
	bool bChange = false;
	uint16_t u16Start = 0;
	uint8_t* pOrg = (uint8_t*) &oData_l;
	uint8_t* pNew = (uint8_t*) &oData;

	loadData(oData_l);

	for (uint16_t i = 0; i < sizeof(s_Data); i++) {
		if (pOrg[i] != pNew[i] && !bChange) {
			bChange = true;
			u16Start = i;
		}
		else if (pOrg[i] == pNew[i] && bChange) {
			bChange = false;
			oM24M01.Write(u16Start, (uint8_t*) &pNew[u16Start], i - u16Start);
		}
	}

	if (bChange) {
		oM24M01.Write(u16Start, (uint8_t*) &pNew[u16Start], sizeof(s_Data) - u16Start);
	}
}

void Settings::writeConfig() {
	oM24M01.Write(0, (uint8_t*) &oData, sizeof(s_Data));
}

