////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "inc/wordclock.hpp"

void Wordclock::drawClock() {
	Colors oColors = oSettings.oData.oColors;
	RTClock::s_RTC oRTCTemp = RTClock::get();
	Animation::AnimationType eSelectedAnimation = oSettings.oData.eAnimation;

	drawClock( {oContainerTmp, oRTCTemp, oColors, oSettings.oData.eMode, true, true, true, true, true, false});

	DisplayMode Mode = oSettings.oData.eMode;
	if (Mode == DisplayMode::Word || Mode == DisplayMode::Word2) {
		int16_t s16Pos = oContainerOld.find(-1, -1);
		if (s16Pos > -1) {
			Point& oPoint = oContainerOld.getRef(s16Pos);
			oContainerTmp.setColor(-1, -1, oPoint.oColors);
			oContainerTmp.setColor(Const::WIDTH, -1, oPoint.oColors);
			oContainerTmp.setColor(-1, Const::HEIGHT, oPoint.oColors);
			oContainerTmp.setColor(Const::WIDTH, Const::HEIGHT, oPoint.oColors);
		}
	}

	if (oRTCTemp.u8Minutes / 5 != oRTCOld.u8Minutes / 5 || abs(RTClock::toTimeStamp(oRTCTemp) - RTClock::toTimeStamp(oRTCOld)) > 5) {
		if (eSelectedAnimation != Animation::AnimationType::None && !AnimationClock.running()) {
			if (eSelectedAnimation == Animation::Random) {
				eSelectedAnimation = (Animation::AnimationType) (std::rand() % Animation::AnimationType::Random);
			}

			AnimationClock = Animation(oContainerOld, oContainerTmp, eSelectedAnimation, oColors, oSettings.oData.eMode);
		}
		oContainerOld = oContainerTmp;
	}

	if (!AnimationClock.running() || eSelectedAnimation == Animation::AnimationType::None) {
		oContainerOld.merge(oContainerTmp);
		oScreen.drawContainer(oContainerOld, getDimmValue());
	}
	else {
		tDisplay.StartMs(AnimationClock.run());
		oScreen.drawContainer(AnimationClock.get(), (uint16_t) AnimationClock.getDimmValue() * getDimmValue() / 0xff);
	}

	oRTCOld = oRTCTemp;
}

void Wordclock::drawSeconds() {
	Colors oColors = oSettings.oData.oColors;
	RTClock::s_RTC oRTCTemp = RTClock::get();
	uint8_t u8DimmValue = getDimmValue();

	if (oRTCOld.u8Seconds / 10 != oRTCTemp.u8Seconds / 10 || oContainerDigit[0].empty()) {
		oContainerDigit[0].clear();
		oContainerDigit[0].drawImage(*Const::aoZiffern[oRTCTemp.u8Seconds / 10], 0, 1, oColors, oSettings.oData.eMode);
	}
	if (oRTCOld.u8Seconds % 10 != oRTCTemp.u8Seconds % 10 || oContainerDigit[1].empty()) {
		oContainerDigit[1].clear();
		oContainerDigit[1].drawImage(*Const::aoZiffern[oRTCTemp.u8Seconds % 10], 6, 1, oColors, oSettings.oData.eMode);
	}
	if (oRTCOld.u8Minutes % 5 != oRTCTemp.u8Minutes % 5 || oContainerOld.empty()) {
		oContainerOld.clear();
		drawDots(oContainerOld, oRTCTemp.u8Minutes % 5, oColors, oSettings.oData.eMode);
	}

	oScreen.drawContainer(oContainerDigit[0], u8DimmValue);
	oScreen.drawContainer(oContainerDigit[1], u8DimmValue);
	oScreen.drawContainer(oContainerOld, u8DimmValue);

	oRTCOld = oRTCTemp;
}

void Wordclock::drawTemp() {
	Colors oColors = oSettings.oData.oColors;

	uint8_t u8DimmValue = getDimmValue();
	uint16_t s16TempRaw = oSI7020.readTempHold();

#ifdef ENABLE_FAHRENHEIT
				s16TempRaw *= 9;
				s16TempRaw /= 5;
				s16TempRaw += 3200;
#endif

	if (s16TempRaw / 1000 != u16TempOld / 1000 || oContainerDigit[0].empty()) {
		oContainerDigit[0].clear();
		oContainerDigit[0].drawImage(*Const::aoZiffern[s16TempRaw / 1000], 0, 1, oColors, oSettings.oData.eMode);

#ifdef ENABLE_TEMP_SYMBOL
		const ClockConfig* oClock = &Const::ClockConfigs[(uint8_t) oSettings.oData.oLanguage];
		oContainerDigit[0].setWord(oClock->TempSymbol[0], oClock->TempSymbol[1], 1, oColors, oSettings.oData.eMode);
#endif
	}
	if ((s16TempRaw / 100) % 10 != (s16TempRaw / 100) % 10 || oContainerDigit[1].empty()) {
		oContainerDigit[1].clear();
		oContainerDigit[1].drawImage(*Const::aoZiffern[(s16TempRaw / 100) % 10], 6, 1, oColors, oSettings.oData.eMode);
	}

	int16_t s16Temp1 = (s16TempRaw % 100) * 5 / 100;
	int16_t s16Temp2 = (u16TempOld & 0x0f) * 5 / 100;
	if (s16Temp1 != s16Temp2 || oContainerOld.empty()) {
		oContainerOld.clear();
		drawDots(oContainerOld, s16Temp1, oColors, oSettings.oData.eMode);
	}

	oScreen.drawContainer(oContainerDigit[0], u8DimmValue);
	oScreen.drawContainer(oContainerDigit[1], u8DimmValue);
	oScreen.drawContainer(oContainerOld, u8DimmValue);

	u16TempOld = s16TempRaw;
}

void Wordclock::drawHumidity() {
	Colors oColors = oSettings.oData.oColors;

	uint8_t u8DimmValue = getDimmValue();
	uint8_t u8Humidity = oSI7020.readHumidityHold();

	if (u8Humidity / 10 != u8HumidityOld / 10 || oContainerDigit[0].empty()) {
		oContainerDigit[0].clear();
		oContainerDigit[0].drawImage(*Const::aoZiffern[u8Humidity / 10], 0, 1, oColors, oSettings.oData.eMode);

#ifdef ENABLE_HUMIDITY_SYMBOL
		const ClockConfig* oClock = &Const::ClockConfigs[(uint8_t) oSettings.oData.oLanguage];
		oContainerDigit[0].setWord(oClock->HumiditySymbol[0], oClock->HumiditySymbol[1], 1, oColors, oSettings.oData.eMode);
#endif
	}
	if (u8Humidity % 10 != u8HumidityOld % 10 || oContainerDigit[1].empty()) {
		oContainerDigit[1].clear();
		oContainerDigit[1].drawImage(*Const::aoZiffern[u8Humidity % 10], 6, 1, oColors, oSettings.oData.eMode);
	}

	oScreen.drawContainer(oContainerDigit[0], u8DimmValue);
	oScreen.drawContainer(oContainerDigit[1], u8DimmValue);

	u8HumidityOld = u8Humidity;
}

void Wordclock::drawDCF77() {
	Colors oColors = oSettings.oData.oColors;

	switch (u8SubMenu) {
		default:
		case 1:
			oColors.setWhiteOnly(0x80);
			oContainerTmp.drawImage(*Const::aoZiffern[abs(oSettings.oData.s8TimeZone) / 10], 0, 0, oColors);
			oContainerTmp.drawImage(*Const::aoZiffern[abs(oSettings.oData.s8TimeZone) % 10], 6, 0, oColors);

			oContainerTmp.setWord(4, 8, 3, oColors);
			if (oSettings.oData.s8TimeZone >= 0) {
				oContainerTmp.setWord(5, 7, 1, oColors);
				oContainerTmp.setWord(5, 9, 1, oColors);
			}

			oScreen.drawContainer(oContainerTmp);
		break;

		case 0:
			oColors.setWhiteOnly(0x80);
			oContainerTmp.drawImage(Const::antenna, 0, 0, oColors);
			oScreen.drawContainer(oContainerTmp);

			if (oSettings.oData.bDCF77) {
				oColors.setGreenOnly(0x80);
			}
			else {
				oColors.setRedOnly(0x80);
			}
			oScreen.drawFrame(oColors);
		break;
	}

	oColors.setWhiteOnly(0x80);
	oScreen.setLED(Const::WIDTH * Const::HEIGHT + u8SubMenu, oColors);
}

void Wordclock::drawHours() {
	Colors oColors = oSettings.oData.oColors;
	RTClock::s_RTC oRTCTemp = RTClock::get();

	bool bDraw = tBlink.RestMs() > Const::u16BlinkTime ? true : false;
	drawClock( {oContainerTmp, oRTCTemp, oColors, DisplayMode::Normal, bDraw, true, true, true, true, true});

	if (tBlink.Ready()) {
		tBlink.StartMs(Const::u16BlinkTime * 2);
	}

	oScreen.drawContainer(oContainerTmp);
}

void Wordclock::drawMinutes() {
	Colors oColors = oSettings.oData.oColors;
	RTClock::s_RTC oRTCTemp = RTClock::get();

	bool bDraw = tBlink.RestMs() > Const::u16BlinkTime ? true : false;
	drawClock( {oContainerTmp, oRTCTemp, oColors, DisplayMode::Normal, true, bDraw, bDraw, true, true, true});

	if (tBlink.Ready()) {
		tBlink.StartMs(Const::u16BlinkTime * 2);
	}

	oScreen.drawContainer(oContainerTmp);
}

void Wordclock::drawColors() {
	uint16_t u16Tmp;
	Colors oColors = oSettings.oData.oColors;

	switch (u8SubMenu) {
		default:
		case 0: {
			RTClock::s_RTC oRTCDraw = RTClock::get();

			drawClock( {oContainerTmp, oRTCDraw, oColors, oSettings.oData.eMode, true, true, false, true, true, false});
			oScreen.drawContainer(oContainerTmp);
		}
		break;

		case 1:
			oScreen.drawCenter(oColors);
			u16Tmp = oColors.getRed();
			oColors.setRedOnly(u16Tmp);
			oScreen.drawFrame(oColors);
			oColors.setRedOnly(0xff);
			oScreen.setLED(5, 2, oColors);
			oColors.setWhiteOnly(0xff);
			for (uint8_t i = 0; i < u16Tmp * 9 / 0xff; i++) {
				oScreen.setLED(1 + i, 8, oColors);
			}
		break;

		case 2:
			oScreen.drawCenter(oColors);
			u16Tmp = oColors.getGreen();
			oColors.setGreenOnly(u16Tmp);
			oScreen.drawFrame(oColors);
			oColors.setGreenOnly(0xff);
			oScreen.setLED(5, 2, oColors);
			oColors.setWhiteOnly(0xff);
			for (uint8_t i = 0; i < u16Tmp * 9 / 0xff; i++) {
				oScreen.setLED(1 + i, 8, oColors);
			}
		break;

		case 3:
			oScreen.drawCenter(oColors);
			u16Tmp = oColors.getBlue();
			oColors.setBlueOnly(u16Tmp);
			oScreen.drawFrame(oColors);
			oColors.setBlueOnly(0xff);
			oScreen.setLED(5, 2, oColors);
			oColors.setWhiteOnly(0xff);
			for (uint8_t i = 0; i < u16Tmp * 9 / 0xff; i++) {
				oScreen.setLED(1 + i, 8, oColors);
			}
		break;

#ifndef USING_WS2812
		case 4:
			oScreen.drawCenter(oColors);
			u16Tmp = oColors.getWhite();
			oColors.setWhiteOnly(u16Tmp);
			oScreen.drawFrame(oColors);
			oColors.setWhiteOnly(0xff);
			oScreen.setLED(5, 2, oColors);
			oColors.setWhiteOnly(0xff);
			for (uint8_t i = 0; i < u16Tmp * 9 / 0xff; i++) {
				oScreen.setLED(1 + i, 8, oColors);
			}
		break;
#endif
	}

	oColors.setWhiteOnly(0x80);
#ifndef USING_WS2812
	uint8_t u8Length = (u8SubMenu + 4) % 5;
#else
				uint8_t u8Length = (u8SubMenu + 3) % 4;
	#endif
	if (u8Length < 4) {
		oScreen.setLED(Const::WIDTH * Const::HEIGHT + u8Length, oColors);
	}
	else {
		oScreen.setLED(Const::WIDTH * Const::HEIGHT, oColors);
		oScreen.setLED(Const::WIDTH * Const::HEIGHT + 3, oColors);
	}
}

void Wordclock::drawLight() {
	Colors oColors = oSettings.oData.oColors;
	uint8_t u8Brightness;
	uint8_t u8Limit;
	const Image* pImage;

	switch (u8SubMenu) {
		default:
		case 0:
		case 1:
			if (u8SubMenu == 0) {
				u8Brightness = oSettings.oData.u8BrightnessMax;
				pImage = &Const::Up;
			}
			else {
				u8Brightness = oSettings.oData.u8BrightnessMin;
				pImage = &Const::Down;
			}

			u8Limit = (uint16_t) Const::WIDTH * u8Brightness / 0xff;

			if (u8Brightness == 0) {
				oColors.setWhiteOnly(0x80);
				oContainerTmp.drawImage(Const::error, 0, 1, oColors);
			}
			else {
				oColors.setWhiteOnly(u8Brightness);
				oContainerTmp.drawImage(*pImage, 2, 0, oColors);
				oContainerTmp.setWord(0, Const::HEIGHT - 1, u8Limit, oColors, DisplayMode::Normal);
			}
			oScreen.drawContainer(oContainerTmp);

			oColors.setWhiteOnly(0x80);
			oScreen.setLED(Const::WIDTH * Const::HEIGHT + u8SubMenu, oColors);
		break;

		case 2:
			oColors.setWhiteOnly(0x80);

			if (oSettings.oData.bEnableLDR) {
				oContainerTmp.drawImage(Const::ok, 0, 1, oColors);
				oColors.setGreenOnly(0x80);
			}
			else {
				oContainerTmp.drawImage(Const::error, 0, 1, oColors);
				oColors.setRedOnly(0x80);
			}
			oScreen.drawFrame(oColors);

			oScreen.drawContainer(oContainerTmp);
			oColors.setWhiteOnly(0x80);
			oScreen.setLED(Const::WIDTH * Const::HEIGHT + u8SubMenu, oColors);
		break;
	}
}

void Wordclock::drawAnimation() {
	Colors oColors = oSettings.oData.oColors;
	RTClock::s_RTC oRTCDraw = RTClock::get();

	drawClock( {oContainerTmp, oRTCDraw, oColors, DisplayMode::Normal, true, true, true, true, true, false});

	if (oSettings.oData.eAnimation != Animation::AnimationType::None && !AnimationMenu.running() && tAnimation.Ready()) {
		AnimationMenu = Animation(oContainerTmp, oContainerTmp, oSettings.oData.eAnimation, oColors);
		tAnimation.StartMs(Const::u32AnimationTime);
	}

	if (oSettings.oData.eAnimation == Animation::AnimationType::Random) {
		oColors.setWhiteOnly(0x80);
		oContainerTmp.clear();
		oContainerTmp.drawImage(Const::question, 2, 0, oColors);
		oScreen.drawContainer(oContainerTmp);
	}
	else if (oSettings.oData.eAnimation == Animation::AnimationType::None) {
		oColors.setWhiteOnly(0x80);
		oContainerTmp.clear();
		oContainerTmp.drawImage(Const::error, 0, 1, oColors);
		oScreen.drawContainer(oContainerTmp);
	}
	else if (AnimationMenu.running()) {
		tDisplay.StartMs(AnimationMenu.run());
		oScreen.drawContainer(AnimationMenu.get(), AnimationMenu.getDimmValue());
	}
	else {
		oScreen.drawContainer(oContainerTmp);
	}
}

void Wordclock::drawNight() {
	Colors oColors = oSettings.oData.oColors;

	oColors.setWhiteOnly(0x80);
	oScreen.setLED(Const::WIDTH * Const::HEIGHT + u8SubMenu, oColors);

	switch (u8SubMenu) {
		default:
		case 2:
			oColors.setWhiteOnly(0x80);

			if (oSettings.oData.bNight) {
				oContainerTmp.drawImage(Const::ok, 0, 1, oColors);
				oColors.setGreenOnly(0x80);
			}
			else {
				oContainerTmp.drawImage(Const::error, 0, 1, oColors);
				oColors.setRedOnly(0x80);
			}
			oScreen.drawFrame(oColors);

			oScreen.drawContainer(oContainerTmp);
		break;

		case 0:
		case 1: {
			RTClock::s_RTC oRTCTemp;
			if (u8SubMenu == 0) {
				oRTCTemp = oSettings.oData.rtcNightOn;
				oColors.setGreenOnly(0x80);
			}
			else {
				oRTCTemp = oSettings.oData.rtcNightOff;
				oColors.setRedOnly(0x80);
			}

			drawClock( {oContainerTmp, oRTCTemp, oColors, DisplayMode::Normal, true, true, false, false, true, true});
			oScreen.drawContainer(oContainerTmp);
		}
		break;
	}
}

void Wordclock::drawLang() {
	Colors oColors = oSettings.oData.oColors;
	const ClockConfig* oClock = &Const::ClockConfigs[(uint8_t) oSettings.oData.oLanguage];
	oContainerTmp.drawImage(*Const::aoBuchstaben[oClock->MenuChars[0] - 'A'], 0, 1, oColors);
	oContainerTmp.drawImage(*Const::aoBuchstaben[oClock->MenuChars[1] - 'A'], 6, 1, oColors);

	oScreen.drawContainer(oContainerTmp);
}

void Wordclock::drawTest() {
	Colors oColors;

	switch (u8SubMenu) {
		default:
		case 0:
			oColors.setRedOnly(0xff);
		break;
		case 1:
			oColors.setGreenOnly(0xff);
		break;
		case 2:
			oColors.setBlueOnly(0xff);
		break;
		case 3:
			oColors.setWhiteOnly(0xff);
		break;
	}

	for (uint16_t i = 0; i < Const::LEDS; i++) {
		oScreen.setLED(i, oColors);
	}
}
