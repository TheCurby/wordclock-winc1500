////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "inc/container.hpp"

Container::Container() {
	clear();
}

void Container::combine(Container& oContainer) {
	for (uint16_t i = 0; i < oContainer.size(); i++) {
		add(oContainer.getRef(i));
	}
}

void Container::merge(Container& oContainer) {
	for (uint16_t i = 0; i < oContainer.size(); i++) {
		Point& oPoint = oContainer.getRef(i);

		if (find(oPoint) == -1) {
			add(oPoint);
		}
	}
}

void Container::clear() {
	u16Length = 0;
}

uint8_t Container::size() const {
	return u16Length;
}

bool Container::areaFree() {
	for (uint16_t i = 0; i < u16Length; i++) {
		if ((aoPoints[i].u8X >= 0 && aoPoints[i].u8X < Const::WIDTH) && (aoPoints[i].u8Y >= 0 && aoPoints[i].u8Y < Const::HEIGHT)) {
			return false;
		}
	}

	return true;
}

bool Container::empty() {
	if (u16Length != 0) {
		return false;
	}
	else {
		return true;
	}
}

void Container::moveX(int16_t s16Val) {
	for (uint16_t i = 0; i < u16Length; i++) {
		aoPoints[i].u8X += s16Val;
	}
}

void Container::moveY(int16_t s16Val) {
	for (uint16_t i = 0; i < u16Length; i++) {
		aoPoints[i].u8Y += s16Val;
	}
}

void Container::add(Point& oPoint) {
	if (u16Length >= u8BufferSize) {
		return;
	}

	aoPoints[u16Length] = oPoint;
	u16Length++;
}

int16_t Container::find(Point& oPoint) {
	return find(oPoint.u8X, oPoint.u8Y);
}

int16_t Container::find(int16_t x, int16_t y) {
	for (uint16_t j = 0; j < u16Length; j++) {
		if (x == aoPoints[j].u8X && y == aoPoints[j].u8Y) {
			return j;
		}
	}

	return -1;
}

void Container::del(Point& oPoint) {
	int16_t s16Pos = find(oPoint);

	if (s16Pos > -1) {
		remove(s16Pos);
	}
}

Point Container::remove(uint16_t u16Pos) {
	if (u16Pos >= u16Length) {
		return aoPoints[0];
	}

	Point oPoint = aoPoints[u16Pos];

	for (uint16_t i = u16Pos; i < u16Length - 1; i++) {
		aoPoints[i] = aoPoints[i + 1];
	}
	u16Length--;

	return oPoint;
}

Point Container::get(uint16_t u16Pos) const {
	return aoPoints[u16Pos];
}

Point& Container::getRef(uint16_t u16Pos) {
	return aoPoints[u16Pos];
}

void Container::setWord(int16_t u8X, int16_t u8Y, uint8_t u8Length_l, Colors& oColors, DisplayMode eMode) {
	Point oPoint;
	oPoint.oColors = oColors;

	if (eMode == DisplayMode::Word || eMode == DisplayMode::Word2) {
		oPoint.oColors.setRandom(eMode);
	}

	for (uint16_t i = 0; i < u8Length_l; i++) {
		if (eMode == DisplayMode::Colorful || eMode == DisplayMode::Colorful2) {
			oPoint.oColors = oColors;
			oPoint.oColors.setRandom(eMode);
		}

		oPoint.u8X = u8X + i;
		oPoint.u8Y = u8Y;

		add(oPoint);
	}
}

void Container::drawImage(const Image& oZ, int16_t u8OffsetX, int16_t u8OffsetY, Colors& oColors_l, DisplayMode eMode) {
	Point oPoint;
	oPoint.oColors = oColors_l;

	if (eMode == DisplayMode::Word || eMode == DisplayMode::Word2) {
		oPoint.oColors.setRandom(eMode);
	}

	for (uint16_t i = 0; i < oZ.u8Size; i++) {
		if (eMode == DisplayMode::Colorful || eMode == DisplayMode::Colorful2) {
			oPoint.oColors = oColors_l;
			oPoint.oColors.setRandom(eMode);
		}

		oPoint.u8X = oZ.u8Points[i].u8X + u8OffsetX;
		oPoint.u8Y = oZ.u8Points[i].u8Y + u8OffsetY;
		add(oPoint);
	}
}

void Container::setColor(int16_t x, int16_t y, Colors& oColors) {
	int16_t s16Pos = find(x, y);

	if (s16Pos >= 0) {
		aoPoints[s16Pos].oColors = oColors;
	}
}

int16_t Container::getRandomWord() {
	int16_t s16Pos = 0;
	uint16_t u16Counter = 0;

	if (u16Length == 0) {
		return 0;
	}

	while (1) {
		u16Counter++;
		int8_t s8Line = rand() % (Const::HEIGHT);

		uint8_t v = rand() % 2;
		if (v == 0) {
			for (int8_t i = 0; i < Const::WIDTH; i++) {
				s16Pos = find(i, s8Line);

				if (s16Pos > -1) {
					return s16Pos;
				}
			}
		}
		else {
			for (int8_t i = Const::WIDTH; i-- > 0;) {
				s16Pos = find(i, s8Line);

				if (s16Pos > -1) {
					return s16Pos;
				}
			}
		}

		if (u16Counter > 100) {
			return 0;
		}
	}

	return 0;
}
