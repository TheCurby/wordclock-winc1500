////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "inc/wordclock.hpp"

void Wordclock::setMenu(Menu Goto) {
	oKeyLeft.setScrolling(false);
	oKeyMiddle.setScrolling(false);
	oKeyRight.setScrolling(false);

	bDir = false;
	u8SubMenu = 0;
	CurrentMenu = Goto;
	forceRedraw();

	switch (Goto) {
		case Menu::Clock:
			AnimationClock.stop();
			oContainerOld.clear();
			oRTCOld = RTClock::get();
		break;

		case Menu::Humidity:
		case Menu::Seconds:
		case Menu::Temp:
			oContainerDigit[0].clear();
			oContainerDigit[1].clear();
			oContainerOld.clear();
			u16TempOld = 0;
			u8HumidityOld = 0;
			oRTCOld = RTClock::get();
		break;

		case Menu::Hours:
		case Menu::Minutes:
			tBlink.StartMs(Const::u16BlinkTime * 2);
			oKeyMiddle.setScrolling(true, 500, 500);
			oKeyRight.setScrolling(true, 500, 500);
		break;

		case Menu::Colors:
			u8SubMenu = 1;
			oKeyRight.setScrolling(true);
		break;

		case Menu::Light:
			oKeyRight.setScrolling(true, 500, 20);
		break;

		case Menu::Night:
			oKeyRight.setScrolling(true, 500, 120);
		break;

		case Menu::Animation:
			AnimationMenu.stop();
			tAnimation.Stop();
		break;

		default:
		break;
	}
}

const Menu Const::MenuMapping[] = {Menu::Clock,
#ifdef ENABLE_DCF77
	Menu::DCF77,
#endif
	Menu::Hours, Menu::Minutes, Menu::Colors,
#ifdef ENABLE_LDR
	Menu::Light,
#endif
#ifdef ENABLE_ANIMATIONS
	Menu::Animation,
#endif
#ifdef ENABLE_NIGHTMODE
	Menu::Night,
#endif
#ifdef ENABLE_LANGUAGE
	Menu::Lang,
#endif
	};

void Wordclock::nextMenu() {
	Menu Goto = Const::MenuMapping[1];
	constexpr uint8_t u8Length = sizeof(Const::MenuMapping) / sizeof(Menu);

	if (CurrentMenu == Menu::Test) {
		Goto = Menu::Clock;
	}
	else {
		for (uint8_t i = 0; i < u8Length; i++) {
			if (Const::MenuMapping[i] == CurrentMenu) {
				if (++i >= u8Length) {
					i = 0;
					oSettings.writeChanges();
				}
				Goto = Const::MenuMapping[i];

				break;
			}
		}
	}

	setMenu(Goto);
	tTimeout.StartMs(Const::u16TimeoutTime);
}

void Wordclock::plusminus(bool bVal) {
	uint8_t u8Temp = 0;

	switch (CurrentMenu) {
		case Menu::Clock:
			if (bVal) {
#ifdef ENABLE_SECONDS
				setMenu(Menu::Seconds);
#elif defined(ENABLE_TEMP)
				setMenu(Menu::Temp);
#elif defined(ENABLE_HUMIDITY)
				setMenu(Menu::Humidity);
#else
				bOn = !bOn;
#endif
			}
			else if (!isNight()) {
				bOn = bOn ? false : true;
			}
		break;

		case Menu::Seconds:
			if (bVal) {
#ifdef ENABLE_TEMP
				setMenu(Menu::Temp);
#elif defined(ENABLE_HUMIDITY)
				setMenu(Menu::Humidity);
#else
				setMenu(Menu::Clock);
#endif
			}
			else if (!isNight()) {
				bOn = bOn ? false : true;
			}
		break;

		case Menu::Temp:
			if (bVal) {
#ifdef ENABLE_HUMIDITY
				setMenu(Menu::Humidity);
#else
				setMenu(Menu::Clock);
#endif
			}
			else if (!isNight()) {
				bOn = bOn ? false : true;
			}
		break;

		case Menu::Humidity:
			if (bVal) {
				setMenu(Menu::Clock);
			}
			else if (!isNight()) {
				bOn = bOn ? false : true;
			}
		break;

		case Menu::DCF77: {
			int8_t s8TimeZone = oSettings.oData.s8TimeZone;

			if (!bVal) {
				switch (u8SubMenu) {
					default:
					case 1:
						s8TimeZone++;
						if (s8TimeZone > 12) {
							s8TimeZone = -12;
						}
						oSettings.oData.s8TimeZone = s8TimeZone;
					break;

					case 0:
						oSettings.oData.bDCF77 = oSettings.oData.bDCF77 ? false : true;
					break;
				}
			}
			else {
				u8SubMenu = (u8SubMenu + 1) % 2;
				if (u8SubMenu == 1) {
					oKeyRight.setScrolling(true);
				}
				else {
					oKeyRight.setScrolling(false);
				}
			}
		}
		break;

		case Menu::Hours: {
			RTClock::s_RTC tRTC = RTClock::get();

			if (bVal) {
				tRTC.u8Hours++;
				if (tRTC.u8Hours >= 24) {
					tRTC.u8Hours = 0;
				}
			}
			else {
				tRTC.u8Hours--;
				if (tRTC.u8Hours >= 24) {
					tRTC.u8Hours = 23;
				}
			}

			RTClock::set(tRTC);
		}
		break;

		case Menu::Minutes: {
			RTClock::s_RTC tRTC = RTClock::get();

			if (bVal) {
				tRTC.u8Minutes++;
				if (tRTC.u8Minutes > 59) {
					tRTC.u8Minutes = 0;

					tRTC.u8Hours++;
					if (tRTC.u8Hours > 23) {
						tRTC.u8Hours = 0;
					}
				}
			}
			else {
				tRTC.u8Minutes--;
				if (tRTC.u8Minutes > 59) {
					tRTC.u8Minutes = 59;

					tRTC.u8Hours--;
					if (tRTC.u8Hours > 23) {
						tRTC.u8Hours = 23;
					}
				}
			}

			tRTC.u8Seconds = 0;
			RTClock::set(tRTC);
		}
		break;

		case Menu::Colors:
			oKeyRight.setScrolling(u8SubMenu != 0 ? true : false);

			if (!bVal) {
				switch (u8SubMenu) {
					default:
					case 0: {
						uint8_t eMode = (uint8_t) oSettings.oData.eMode;
						eMode += 1;

						if (eMode >= DisplayMode::COUNT_MODE) {
							eMode = DisplayMode::Normal;
						}

						oSettings.oData.eMode = (DisplayMode) eMode;
					}
					break;

					case 1:
						u8Temp = oSettings.oData.oColors.getRed();
						if (u8Temp < 0xff) {
							oSettings.oData.oColors.setRed(u8Temp + 1);
						}
						if (oKeyRightDouble.getLastStatus()) {
							oSettings.oData.oColors.setRed(0);
						}
					break;

					case 2:
						u8Temp = oSettings.oData.oColors.getGreen();
						if (u8Temp < 0xff) {
							oSettings.oData.oColors.setGreen(u8Temp + 1);
						}
						if (oKeyRightDouble.getLastStatus()) {
							oSettings.oData.oColors.setGreen(0);
						}
					break;

					case 3:
						u8Temp = oSettings.oData.oColors.getBlue();
						if (u8Temp < 0xff) {
							oSettings.oData.oColors.setBlue(u8Temp + 1);
						}
						if (oKeyRightDouble.getLastStatus()) {
							oSettings.oData.oColors.setBlue(0);
						}
					break;

#ifndef USING_WS2812
					case 4:
						u8Temp = oSettings.oData.oColors.getWhite();
						if (u8Temp < 0xff) {
							oSettings.oData.oColors.setWhite(u8Temp + 1);
						}
						if (oKeyRightDouble.getLastStatus()) {
							oSettings.oData.oColors.setWhite(0);
						}
					break;
#endif
				}
			}
			else {
#ifndef USING_WS2812
				u8SubMenu = (u8SubMenu + 1) % 5;
#else
				u8SubMenu = (u8SubMenu + 1) % 4;
#endif
			}
		break;

		case Menu::Light:
			if (!bVal) {
				switch (u8SubMenu) {
					default:
					case 0:
						u8Temp = oSettings.oData.u8BrightnessMax;
						oSettings.oData.u16BrightnessOffsetMin = u16ADC;

						if (u8Temp > 0) {
							oSettings.oData.u8BrightnessMax = u8Temp - 1;
						}

						if (oKeyRightDouble.getLastStatus()) {
							oSettings.oData.u8BrightnessMax = 0xff;
						}
					break;

					case 1:
						u8Temp = oSettings.oData.u8BrightnessMin;
						oSettings.oData.u16BrightnessOffsetMax = u16ADC;

						if (u8Temp > 0) {
							oSettings.oData.u8BrightnessMin = u8Temp - 1;
						}

						if (oKeyRightDouble.getLastStatus()) {
							oSettings.oData.u8BrightnessMin = 0xff;
						}
					break;

					case 2:
						oSettings.oData.bEnableLDR = oSettings.oData.bEnableLDR ? false : true;
					break;
				}
			}
			else {
				u8SubMenu = (u8SubMenu + 1) % 3;
			}
		break;

		case Menu::Animation: {
			uint8_t eSelectedAnimation = oSettings.oData.eAnimation;

			if (bVal) {
				eSelectedAnimation += (Animation::AnimationType) 1;
				if (eSelectedAnimation > Animation::AnimationType::None) {
					eSelectedAnimation = 0;
				}
			}
			else {
				eSelectedAnimation -= (Animation::AnimationType) 1;
				if (eSelectedAnimation > Animation::AnimationType::None) {
					eSelectedAnimation = Animation::AnimationType::None;
				}
			}

			oSettings.oData.eAnimation = (Animation::AnimationType) eSelectedAnimation;
			AnimationMenu.stop();
			tAnimation.Stop();
		}
		break;

		case Menu::Night: {
			if (!bVal) {
				switch (u8SubMenu) {
					default:
					case 2:
						oSettings.oData.bNight = oSettings.oData.bNight ? false : true;
					break;

					case 0:
					case 1:
						RTClock::s_RTC rtcNight;

						if (oKeyRightDouble.getLastStatus()) {
							bDir = bDir ? false : true;
						}

						if (u8SubMenu == 0) {
							rtcNight = oSettings.oData.rtcNightOn;
						}
						else {
							rtcNight = oSettings.oData.rtcNightOff;
						}

						rtcNight.u8Minutes -= rtcNight.u8Minutes % 5;
						if (!bDir) {
							rtcNight.u8Minutes += 5;
							if (rtcNight.u8Minutes > 59) {
								rtcNight.u8Minutes = 0;

								if (++rtcNight.u8Hours > 23) {
									rtcNight.u8Hours = 0;
								}
							}
						}
						else {
							rtcNight.u8Minutes -= 5;
							if (rtcNight.u8Minutes > 59) {
								rtcNight.u8Minutes = 55;

								if (--rtcNight.u8Hours > 23) {
									rtcNight.u8Hours = 23;
								}
							}
						}

						if (u8SubMenu == 0) {
							oSettings.oData.rtcNightOn = rtcNight;
						}
						else {
							oSettings.oData.rtcNightOff = rtcNight;
						}
					break;
				}
			}
			else {
				bDir = false;
				u8SubMenu = (u8SubMenu + 1) % 3;

				if (u8SubMenu == 2) {
					oKeyRight.setScrolling(false, 500, 120);
				}
				else {
					oKeyRight.setScrolling(true, 500, 120);
				}
			}
		}
		break;

		case Menu::Lang: {
			uint8_t oLang = oSettings.oData.oLanguage;

			if (!bVal) {
				oLang -= 1;
				if (oLang >= Language::COUNT_LANG) {
					oLang = Language::COUNT_LANG - 1;
				}
			}
			else {
				oLang += 1;
				if (oLang >= Language::COUNT_LANG) {
					oLang = 0;
				}
			}

			oSettings.oData.oLanguage = (Language) oLang;
		}
		break;

		case Menu::Test:
			if (bVal) {
				u8SubMenu = (u8SubMenu + 1) % 4;
			}
			else {
				if (u8SubMenu == 0) {
					u8SubMenu = 3;
				}
				else {
					u8SubMenu--;
				}
			}
		break;

		default:
			return;
		break;
	}

	forceRedraw();
	tTimeout.StartMs(Const::u16TimeoutTime);
}

void Wordclock::getKeys() {
	bool bVal;

	bVal = STM32::getPin(KEYLEFT_PORT, KEYLEFT_PIN) ? false : true;
	oKeyLeft.getSingle(bVal);
	oKeyTest.getLongPress(oKeyLeft.getValue(), 5000);

	bVal = STM32::getPin(KEYMIDDLE_PORT, KEYMIDDLE_PIN) ? false : true;
	oKeyMiddle.getSingle(bVal);

	bVal = STM32::getPin(KEYRIGHT_PORT, KEYRIGHT_PIN) ? false : true;
	oKeyRight.getSingle(bVal);
	oKeyRightDouble.getDouble(oKeyRight.getValue());
}
