#!/usr/bin/python

import os

result = "#include \"inc/htmlfiles.hpp\"\n\n"
nFiles = 0
files = ""

def add_file(dir, header, entry):
	global result
	global files
	global nFiles
	
	len = 0
	tmp = ""
	fh = open(dir + "/" + entry, 'rb')
	
	filename = os.path.splitext(entry)[-2]
	extension = os.path.splitext(entry)[-1]
	files += "&f_" + str(nFiles) + ","

	ba = bytearray(fh.read())
	for byte in ba:
		len+=1
		tmp = tmp + str(hex(byte)) + ","
		
		if len % 24 == 0:
			tmp += "\n\t"
	tmp = tmp[:-1]
	
	if extension == ".js":
		extension = "application/javascript"
	elif extension == ".css":
		extension = "text/css"
	elif extension == ".jpeg":
		extension = "image/jpeg"
	elif extension == ".jpg":
		extension = "image/jpeg"
	elif extension == ".gif":
		extension = "image/gif"
	elif extension == ".png":
		extension = "image/png"
	elif extension == ".ico":
		extension = "image/x-icon"
	elif extension == ".svg":
		extension = "image/svg+xml"
	else :
		extension = "text/html"
	
	#add File
	result += "static const file f_" + str(nFiles) + " = {\n"
	result += "\t.name = (const char*)\"" + header + "/" + entry + "\",\n"
	result += "\t.contentType = (const char*)\"" + str(extension) + "\",\n"
	result += "\t.u32Size = " + str(len) + ",\n"
	result += "\t.b = {" + tmp + "}\n"
	result += "};\n\n"
	
	fh.close()

def scan_dir(header):
	global nFiles
	obj = os.scandir("../html" + header)
	
	for entry in obj:
		if entry.is_file():
			nFiles += 1
			add_file("../html" + header, header, entry.name)
			
		if entry.is_dir():
			scan_dir("/" + entry.name)
			
scan_dir("")
		
#pointer array and size
result += "const uint8_t nFiles = " + str(nFiles) + ";\n\n"

files = files[:-1]
result += "const file* arrFiles[] = {" + files + "};"
		
		
f = open("../wifi/src/htmlfiles.cpp", "w")
f.write(result)
f.close()