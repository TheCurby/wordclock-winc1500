////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#pragma once

#define MAIN_WIFI_M2M_BUFFER_SIZE M2M_BUFFER_MAX_SIZE
#define MAX_SOCKETS 10

#include <cstring>
extern "C" {
#include "driver/include/m2m_wifi.h"
#include "socket/include/socket.h"
}

class Socket {
	public:
		enum Type {
			UDP,
			TCP_SERVER,
			TCP_CLIENT
		};

		enum State {
			IDLE,
			CONNECTING,
			CONNECTED
		};

		Socket();
		~Socket();
		void _open(uint16_t u16Port, uint32_t s_addr, uint8_t u8Type, void (*read)(Socket&, tstrSocketRecvMsg&) = nullptr, void (*close)() = nullptr);
		void _send(char* sTxt, struct sockaddr* addr, void (*callback)(Socket&) = nullptr);
		void _send(uint8_t* pu8Data, uint32_t u32Size, struct sockaddr* addr, void (*callback)(Socket&) = nullptr);
		void _connect();
		void _close();

		bool isOpened();
		bool isPort(uint16_t u16Port);
		uint8_t getType();
		uint8_t u8getState();

		static void socket_cb(SOCKET sock, uint8_t u8Msg, void* pvMsg);
		static void closeDynamicSockets();
		static uint8_t nConnections();
		static bool ConnectionLimit();

	private:
		static uint8_t pUDPBuffer[MAIN_WIFI_M2M_BUFFER_SIZE];
		static uint8_t pTCPBuffer[MAIN_WIFI_M2M_BUFFER_SIZE];
		static uint8_t nSocketsConnected;
		static Socket* arrSocketsConnected[MAX_SOCKETS];
		static uint8_t nSocketsDynamic;

		struct {
				bool bPending;
				uint32_t u32Size;
				uint8_t* pu8Data;
				struct sockaddr addr;
		} oPending;
		SOCKET sock;
		bool bDynamic;
		uint16_t u16Port;
		uint8_t u8Type;
		uint8_t u8State;
		struct sockaddr_in addr;

		SOCKET getSocket();
		void pending();
		bool isSocket(SOCKET sock);
		void resetSocket();
		void (*cb_read)(Socket&, tstrSocketRecvMsg&);
		void (*cb_send)(Socket&);
		void (*cb_close)();

		static bool removeSocket(SOCKET sock);
};
