////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <stdio.h>
#include "driver/spi.hpp"
#include "inc/HTTPParser.hpp"
#include "inc/wordclock.hpp"
#include "inc/socket.hpp"
extern "C" {
#include "driver/include/m2m_wifi.h"
}

#define CONNECTION_TIMEOUT 500
#define CONNECTION_TIMEOUT_RESTART 2 * 60 * 1000
#define PORT_UDP_BROADCAST 23000
#define PORT_TCP_APPSERVER 502
#define PORT_HTTP 80
#define PORT_HTTP_SSL 443
#define UDP_IDENTIFY "identify"
#define UDP_REQUEST "WORDCLOCK_WINC1500"
#define UDP_RESPONSE "IAMHERE!"

extern Wordclock oWordclock;

class Wifi {
	public:
		enum class Status {
			DISCONNECTED,
			ACCESSPOINT_INIT,
			CONNECTED_INIT,
			CONNECTED,
			ACCESSPOINT,

			ESTABLISH_ACCESSPOINT,
			ESTABLISH_CONNECTION,
			DICONNECTING,

			IDLE
		};

		static void init();
		static void loop();
		static void open_sockets();

		static SPI oSPI;

	private:
		static bool bForceReconnect;
		static Status u8ConnectionStatus;
		static Timer tTimeoutConnection;
		static Timer tRestart;

		static Socket udpBroadcast;
		static Socket HTTPServer;
		static Socket AppServer;
		static char json[1000];

		static void createJson();
		static void processJson(char* sJson, bool bSaveinstant = true);

		static void close_all_sockets();
		static void close_connection();
		static void setHostname();

		static void connectToWifi(char* ssid, char* key);
		static void createAP(char* ssid, char* key);
		static void wifi_cb(uint8_t u8MsgType, void* pvMsg);

		static int8_t strcmp(uint8_t* pu8Src, const char* sCmp);
		static uint32_t ipStringToNumber(const char* pDottedQuad);
};

