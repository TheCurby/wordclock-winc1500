////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "inc/HTTPParser.hpp"

char sEmpty = '0';

HTTPParser::HTTPParser(const char* str) {
	sHeader = str;
	sPos = str;
	bIsBody = false;

	nextHeader();
}

const char* HTTPParser::getMethod() {
	return sHeader;
}

const char* HTTPParser::getRessource() {
	const char* sPos_l = sHeader;
	constexpr char* sDefault = (char*) "/index.html";

	/*jump to Ressource Header*/
	while (*sPos_l != ' ' && *sPos_l != '\n' && *sPos_l != '\0') {
		sPos_l++;
	}

	switch (*sPos_l) {
		case ' ':
			sPos_l++;
			trim((char*) sPos_l);

			if (strcmp((uint8_t*) sPos_l, "/ ") == 0) {
				return sDefault;
			}

			return sPos_l;
		break;

		default:
			return &sEmpty;
		break;
	}
}

bool HTTPParser::nextHeader() {
	/*jump to first Header*/
	while (*sPos != '\n' && *sPos != '\0') {
		sPos++;
	}

	switch (*sPos) {
		case '\0':
			return false;
		break;

		case '\n':
			sPos++;
			trim((char*) sPos);

			if (*sPos == '\0') {
				return false;
			}
			else if (*sPos == '\r') {
				while (*sPos == '\r' || *sPos == '\n') {
					sPos++;
				}

				bIsBody = true;
				return false;
			}

			return true;
		break;

		default:
			return false;
		break;
	}
}

const char* HTTPParser::getHeader() {
	switch (*sPos) {
		case '\0':
			return &sEmpty;
		break;

		default:
			return sPos;
		break;
	}
}

const char* HTTPParser::getHeaderValue() {
	const char* sTmp = sPos;

	while (*sTmp != '\n' && *sTmp != '\0' && *sTmp != ':') {
		sTmp++;
	}

	switch (*sTmp) {
		case '\n':
			return &sEmpty;
		break;

		case ':':
			sTmp++;
			trim((char*) sTmp);
			return sTmp;
		break;

		default:
		case '\0':
			return &sEmpty;
		break;
	}
}

bool HTTPParser::isRessource(const char* sStr) {
	const char* sRessource = getRessource();

	if (strcmp((uint8_t*) sRessource, sStr) == 0) {
		return true;
	}

	return false;
}

bool HTTPParser::isBody() {
	return bIsBody;
}

const char* HTTPParser::getBody() {
	if (*sPos == '\0') {
		return &sEmpty;
	}
	else {
		return sPos;
	}
}

void HTTPParser::trim(char* sStr) {
	while (*sStr == ' ' || *sStr == '\t') {
		sStr++;
	}
}

int8_t HTTPParser::strcmp(uint8_t* pu8Src, const char* sCmp) {
	while (*sCmp != '\0') {
		if (*sCmp != *((char*) pu8Src)) {
			return -1;
		}
		pu8Src++;
		sCmp++;
	}

	return 0;
}
