////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "inc/wifi.hpp"

void Wifi::wifi_cb(uint8_t u8MsgType, void* pvMsg) {
	switch (u8MsgType) {
		case M2M_WIFI_RESP_CONN_INFO: {
			/*tstrM2MProvisionInfo* pstrProvInfo = (tstrM2MProvisionInfo*) pvMsg;

			if (pstrProvInfo->u8Status == M2M_SUCCESS) {

			}
			else {
				u8ConnectionStatus = Status::DISCONNECTED;
				tTimeoutConnection.StartMs(CONNECTION_TIMEOUT);
			}*/
		}
		break;

		case M2M_WIFI_RESP_CON_STATE_CHANGED: {
			tstrM2mWifiStateChanged* pstrWifiState = (tstrM2mWifiStateChanged*) pvMsg;

			if (pstrWifiState->u8CurrState == M2M_WIFI_CONNECTED) {
				switch (u8ConnectionStatus) {
					case Status::ESTABLISH_ACCESSPOINT:
						u8ConnectionStatus = Status::ACCESSPOINT_INIT;
					break;

					case Status::ESTABLISH_CONNECTION:
						u8ConnectionStatus = Status::CONNECTED_INIT;
					break;

					default:
					break;
				}
			}
			else if (pstrWifiState->u8CurrState == M2M_WIFI_DISCONNECTED) {
				switch (u8ConnectionStatus) {
					case Status::CONNECTED_INIT:
					case Status::CONNECTED:
						close_all_sockets();
						u8ConnectionStatus = Status::DISCONNECTED;
						tTimeoutConnection.StartMs(CONNECTION_TIMEOUT);
					break;

					case Status::DICONNECTING:
						u8ConnectionStatus = Status::DISCONNECTED;
						tTimeoutConnection.StartMs(CONNECTION_TIMEOUT);
					break;

					case Status::ESTABLISH_CONNECTION:
						u8ConnectionStatus = Status::DISCONNECTED;

						char ssid[20];
						sprintf(ssid, "wordclock_%u", rand() % 1000);
						tTimeoutConnection.StartMs(CONNECTION_TIMEOUT_RESTART);
						createAP(ssid, nullptr);
					break;

					default:
					break;
				}
			}
		}
		break;

		case M2M_WIFI_RESP_GET_SYS_TIME: {
			tstrSystemTime* pstrSystemTime = (tstrSystemTime*) pvMsg;
			if (oWordclock.oSettings.oData.bFetchInternetTime) {
				RTClock::s_RTC oRTC;
				oRTC.u8Day = pstrSystemTime->u8Day;
				oRTC.u8Month = pstrSystemTime->u8Month;
				oRTC.u8Year = pstrSystemTime->u16Year;
				oRTC.u8Hours = pstrSystemTime->u8Hour;
				oRTC.u8Minutes = pstrSystemTime->u8Minute;
				oRTC.u8Seconds = pstrSystemTime->u8Second;
				RTClock::set(oRTC);
			}
		}
		break;

		default:
		break;
	}
}

void Wifi::setHostname() {
	m2m_wifi_set_device_name((uint8_t*) oWordclock.oSettings.oData.deviceName, strlen(oWordclock.oSettings.oData.deviceName));
}

void Wifi::createAP(char* ssid, char* key) {
	int8_t ret;
	tstrM2MAPModeConfig config;

	if (u8ConnectionStatus != Status::DISCONNECTED) {
		return;
	}

	strcpy((char*) config.strApConfig.au8SSID, ssid);
	if (key != nullptr) {
		config.strApConfig.u8SecType = M2M_WIFI_SEC_WEP;
		strcpy((char*) config.strApConfig.au8Key, key);
		config.strApConfig.u8KeySz = strlen(key);
	}
	else {
		config.strApConfig.u8SecType = M2M_WIFI_SEC_OPEN;
	}
	config.strApConfig.u8SsidHide = 0;
	config.strApConfig.au8DHCPServerIP[0] = 192;
	config.strApConfig.au8DHCPServerIP[1] = 168;
	config.strApConfig.au8DHCPServerIP[2] = 1;
	config.strApConfig.au8DHCPServerIP[3] = 1;
	config.strApConfig.u8ListenChannel = 1;

	// Default router IP
	m2m_memcpy(config.strApConfigExt.au8DefRouterIP, config.strApConfig.au8DHCPServerIP, 4);
	// DNS Server IP
	m2m_memcpy(config.strApConfigExt.au8DNSServerIP, config.strApConfig.au8DHCPServerIP, 4);
	// Subnet mask
	config.strApConfigExt.au8SubnetMask[0] = 255;
	config.strApConfigExt.au8SubnetMask[1] = 255;
	config.strApConfigExt.au8SubnetMask[2] = 255;
	config.strApConfigExt.au8SubnetMask[3] = 0;

	ret = m2m_wifi_start_provision_mode_ext(&config, (char*) "wordclockLAN", 0);
	if (ret == M2M_SUCCESS) {
		u8ConnectionStatus = Status::ESTABLISH_ACCESSPOINT;
	}
	else {
		tTimeoutConnection.StartMs(CONNECTION_TIMEOUT);
	}
}

/*void Wifi::createAP(char* ssid, char* key) {
 int8_t ret;
 tstrM2MAPConfig config;

 if (u8ConnectionStatus != Status::DISCONNECTED) {
 return;
 }

 strcpy((char*) config.au8SSID, ssid);
 if (key != nullptr) {
 config.u8SecType = M2M_WIFI_SEC_WEP;
 strcpy((char*) config.au8Key, key);
 config.u8KeySz = strlen(key);
 }
 else {
 config.u8SecType = M2M_WIFI_SEC_OPEN;
 }
 config.u8SsidHide = 0;
 config.au8DHCPServerIP[0] = 192;
 config.au8DHCPServerIP[1] = 168;
 config.au8DHCPServerIP[2] = 1;
 config.au8DHCPServerIP[3] = 1;
 config.u8ListenChannel = 1;

 ret = m2m_wifi_enable_ap(&config);
 if (ret == M2M_SUCCESS) {
 u8ConnectionStatus = Status::ACCESSPOINT_INIT;
 tRestart.StartMs(CONNECTION_TIMEOUT);
 }
 else {
 close_connection();
 tTimeoutConnection.StartMs(CONNECTION_TIMEOUT);
 }
 }*/

void Wifi::connectToWifi(char* ssid, char* key) {
	int8_t ret;
	tstrNetworkId network;
	tstrAuthPsk wpakey;

	if (u8ConnectionStatus != Status::DISCONNECTED) {
		return;
	}

	network.enuChannel = M2M_WIFI_CH_ALL;
	network.pu8Bssid = nullptr;
	network.pu8Ssid = (uint8_t*) ssid;
	network.u8SsidLen = strlen(ssid);

	wpakey.pu8Passphrase = (uint8_t*) key;
	wpakey.pu8Psk = nullptr;
	wpakey.u8PassphraseLen = strlen(key);

	ret = m2m_wifi_connect_psk(WIFI_CRED_DONTSAVE, &network, &wpakey);
	if (ret == M2M_SUCCESS) {
		u8ConnectionStatus = Status::ESTABLISH_CONNECTION;
	}
	else {
		tTimeoutConnection.StartMs(CONNECTION_TIMEOUT);
	}
}

void Wifi::close_all_sockets() {
	udpBroadcast._close();
	HTTPServer._close();
	AppServer._close();
	Socket::closeDynamicSockets();
}

void Wifi::close_connection() {
	close_all_sockets();
	if (u8ConnectionStatus == Status::CONNECTED || u8ConnectionStatus == Status::CONNECTED_INIT) {
		m2m_wifi_disconnect();
		u8ConnectionStatus = Status::DICONNECTING;
	}
	else if (u8ConnectionStatus == Status::ACCESSPOINT || u8ConnectionStatus == Status::ACCESSPOINT_INIT) {
		m2m_wifi_stop_provision_mode();
		u8ConnectionStatus = Status::DISCONNECTED;
		tTimeoutConnection.StartMs(CONNECTION_TIMEOUT);
	}
}

uint32_t Wifi::ipStringToNumber(const char* pDottedQuad) {
	unsigned int byte3;
	unsigned int byte2;
	unsigned int byte1;
	unsigned int byte0;
	char dummyString[2];
	uint32_t pIpAddr;

	if (sscanf(pDottedQuad, "%u.%u.%u.%u%1s", &byte3, &byte2, &byte1, &byte0, dummyString) == 4) {
		if ((byte3 < 256) && (byte2 < 256) && (byte1 < 256) && (byte0 < 256)) {
			pIpAddr = (byte3 << 24) + (byte2 << 16) + (byte1 << 8) + byte0;

			return _htonl(pIpAddr);
		}
	}

	return 0;
}

int8_t Wifi::strcmp(uint8_t* pu8Src, const char* sCmp) {
	while (*sCmp != '\0') {
		if (*sCmp != *((char*) pu8Src)) {
			return -1;
		}
		pu8Src++;
		sCmp++;
	}

	return 0;
}
