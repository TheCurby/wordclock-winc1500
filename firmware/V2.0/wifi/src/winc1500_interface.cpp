////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "bsp/include/cpp_interface.h"
#include "inc/wifi.hpp"

extern Wordclock oWordclock;

extern "C" {
	void clrPin(GPIO_TypeDef* oGPIO, uint8_t u8Pin) {
		STM32::clrPin(oGPIO, u8Pin);
	}

	void setPin(GPIO_TypeDef* oGPIO, uint8_t u8Pin) {
		STM32::setPin(oGPIO, u8Pin);
	}

	uint8_t sendSPI(uint8_t u8Val){
		return Wifi::oSPI.send(u8Val);
	}

	void delay_ms(uint16_t ms) {
		Timer t;

		t.StartMs(ms);
		while (!t.Ready());
	}

	void enableInterrupt(uint8_t u8Enable){
		if (!u8Enable) {
			EXTI->FTSR1 &= ~EXTI_FTSR1_FT7;
			EXTI->FPR1 |= EXTI_FPR1_FPIF0;
		}
		else {
			EXTI->FPR1 |= EXTI_FPR1_FPIF0;
			EXTI->FTSR1 |= EXTI_FTSR1_FT7;
		}
	}
}
