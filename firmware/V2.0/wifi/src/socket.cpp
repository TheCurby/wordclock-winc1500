////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "inc/socket.hpp"

uint8_t Socket::pUDPBuffer[MAIN_WIFI_M2M_BUFFER_SIZE];
uint8_t Socket::pTCPBuffer[MAIN_WIFI_M2M_BUFFER_SIZE];
Socket* Socket::arrSocketsConnected[MAX_SOCKETS];
uint8_t Socket::nSocketsConnected = 0;
uint8_t Socket::nSocketsDynamic = 0;

Socket::Socket() {
	resetSocket();
}

Socket::~Socket() {
	if (sock > -1) {
		_close();
	}
}

void Socket::_open(uint16_t u16Port, uint32_t s_addr, uint8_t u8Type, void (*read)(Socket&, tstrSocketRecvMsg&), void (*close)()) {
	if (sock >= 0) {
		return;
	}

	resetSocket();

	if (nSocketsConnected >= MAX_SOCKETS) {
		return;
	}

	if (u8Type == UDP) {
		sock = socket(AF_INET, SOCK_DGRAM, 0);
	}
	else {
		sock = socket(AF_INET, SOCK_STREAM, 0);
	}

	if (sock >= 0) {
		/* Socket bind */
		addr.sin_family = AF_INET;
		addr.sin_port = _htons(u16Port);
		addr.sin_addr.s_addr = s_addr;
		this->u16Port = u16Port;
		this->cb_read = read;
		this->cb_close = close;
		this->u8Type = u8Type;
		u8State = IDLE;

		arrSocketsConnected[nSocketsConnected++] = this;

		if (read && u8Type != TCP_CLIENT) {
			bind(sock, (struct sockaddr*) &addr, sizeof(struct sockaddr_in));
		}
	}
	else {
		resetSocket();
	}
}

void Socket::_connect() {
	if (u8Type != TCP_CLIENT) {
		return;
	}

	if (u8State == IDLE) {
		u8State = CONNECTING;
		connect(sock, (struct sockaddr*) &addr, sizeof(struct sockaddr_in));
	}
}

void Socket::_send(char* sTxt, struct sockaddr* addr, void (*callback)(Socket&)) {
	_send((uint8_t*) sTxt, strlen(sTxt), addr, callback);
}

void Socket::_send(uint8_t* pu8Data, uint32_t u32Size, struct sockaddr* addr, void (*callback)(Socket&)) {
	cb_send = callback;

	if (u32Size < SOCKET_BUFFER_MAX_LENGTH) {
		oPending.bPending = false;

		if (u8Type == UDP) {
			sendto(sock, (void*) pu8Data, u32Size, 0, addr, sizeof(addr));
		}
		else {
			send(sock, (void*) pu8Data, u32Size, 0);
		}
	}
	else {
		oPending.bPending = true;
		oPending.pu8Data = pu8Data;
		oPending.u32Size = u32Size;
		oPending.addr = *addr;

		if (u8Type == UDP) {
			sendto(sock, (void*) pu8Data, SOCKET_BUFFER_MAX_LENGTH, 0, addr, sizeof(addr));
		}
		else {
			send(sock, (void*) pu8Data, SOCKET_BUFFER_MAX_LENGTH, 0);
		}
	}
}

void Socket::pending() {
	uint32_t u32Size;

	oPending.u32Size -= SOCKET_BUFFER_MAX_LENGTH;
	oPending.pu8Data += SOCKET_BUFFER_MAX_LENGTH;

	if (oPending.u32Size <= SOCKET_BUFFER_MAX_LENGTH) {
		oPending.bPending = false;
		u32Size = oPending.u32Size;
	}
	else {
		u32Size = SOCKET_BUFFER_MAX_LENGTH;
	}

	if (u8Type == UDP) {
		sendto(sock, (void*) oPending.pu8Data, u32Size, 0, &oPending.addr, sizeof(oPending.addr));
	}
	else {
		send(sock, (void*) oPending.pu8Data, u32Size, 0);
	}
}

SOCKET Socket::getSocket() {
	return sock;
}

uint8_t Socket::getType() {
	return u8Type;
}

uint8_t Socket::u8getState() {
	return u8State;
}

bool Socket::isSocket(SOCKET sock) {
	if (this->sock == sock) {
		return true;
	}

	return false;
}

bool Socket::isOpened() {
	if (sock > -1) {
		return true;
	}

	return false;
}

bool Socket::isPort(uint16_t u16Port) {
	if (this->u16Port == u16Port) {
		return true;
	}

	return false;
}

void Socket::resetSocket() {
	sock = -1;
	u16Port = 0;
	u8State = IDLE;
	cb_read = nullptr;
	cb_send = nullptr;
	cb_close = nullptr;
	bDynamic = false;
}

void Socket::closeDynamicSockets() {
	for (uint8_t i = nSocketsConnected; i-- > 0;) {
		if (arrSocketsConnected[i]->bDynamic) {
			arrSocketsConnected[i]->_close();
		}
	}
}

void Socket::_close() {
	close(sock);

	if (cb_close) {
		cb_close();
	}

	bool bFound = removeSocket(sock);
	if (!bFound) {
		resetSocket();
	}
}

bool Socket::removeSocket(SOCKET sock) {
	bool bFound = false;

	for (uint8_t i = 0; i < nSocketsConnected; i++) {
		if (arrSocketsConnected[i]->isSocket(sock)) {
			if (arrSocketsConnected[i]->bDynamic) {
				free(arrSocketsConnected[i]);
				bFound = true;
				nSocketsDynamic--;
			}

			nSocketsConnected--;
			for (uint8_t j = i; j < nSocketsConnected; j++) {
				arrSocketsConnected[j] = arrSocketsConnected[j + 1];
			}

			break;
		}
	}

	/*uint8_t i;
	 for (i = 0; i < nSocketsDynamic; i++) {
	 if (arrSocketsDynamic[i].isSocket(sock)) {
	 bFound = true;

	 nSocketsDynamic--;
	 for (uint8_t j = i; j < nSocketsDynamic; j++) {
	 for (uint8_t n = 0; n < nSocketsConnected; n++) {
	 if (arrSocketsConnected[n]->isSocket(arrSocketsDynamic[j + 1].sock)) {
	 arrSocketsConnected[n]--;
	 break;
	 }
	 }

	 arrSocketsDynamic[j] = arrSocketsDynamic[j + 1];
	 }
	 break;
	 }
	 }*/

	return bFound;
}

uint8_t Socket::nConnections() {
	return nSocketsDynamic;
}

bool Socket::ConnectionLimit() {
	if (nSocketsConnected >= MAX_SOCKETS) {
		return true;
	}

	return false;
}

void Socket::socket_cb(SOCKET sock, uint8_t u8Msg, void* pvMsg) {
	Socket* pSocket = nullptr;

	for (uint8_t i = 0; i < nSocketsConnected; i++) {
		if (arrSocketsConnected[i]->isSocket(sock)) {
			pSocket = arrSocketsConnected[i];
			break;
		}
	}

	if (!pSocket) {
		return;
	}

	switch (u8Msg) {
		case SOCKET_MSG_BIND: {
			tstrSocketBindMsg* pstrBind = (tstrSocketBindMsg*) pvMsg;

			if (pstrBind && pstrBind->status == 0) {
				if (pSocket->getType() == Socket::UDP) {
					if (pSocket->cb_read) {
						recvfrom(sock, pUDPBuffer, MAIN_WIFI_M2M_BUFFER_SIZE, 0);
					}
				}
				else {
					listen(sock, 0);
				}
			}
			else {
				pSocket->_close();
			}
		}
		break;

		case SOCKET_MSG_LISTEN: {
			tstrSocketListenMsg* pstrListen = (tstrSocketListenMsg*) pvMsg;

			if (pstrListen && pstrListen->status == 0) {
				accept(sock, NULL, NULL);
			}
			else {
				pSocket->_close();
			}
		}
		break;

		case SOCKET_MSG_ACCEPT: {
			tstrSocketAcceptMsg* pstrAccept = (tstrSocketAcceptMsg*) pvMsg;

			if (pstrAccept) {
				accept(sock, NULL, NULL);

				/*remove dead sockets*/
				for (uint8_t i = 0; i < nSocketsConnected; i++) {
					if (arrSocketsConnected[i]->isSocket(pstrAccept->sock)) {
						removeSocket(pstrAccept->sock);

						break;
					}
				}

				if (!ConnectionLimit() && pstrAccept->sock >= 0) {
					Socket* p = (Socket*) malloc(sizeof(Socket));
					*p = *pSocket;
					p->sock = pstrAccept->sock;
					p->u16Port = _ntohs(pstrAccept->strAddr.sin_port);
					p->bDynamic = true;
					recv(pstrAccept->sock, pTCPBuffer, MAIN_WIFI_M2M_BUFFER_SIZE, 0);

					arrSocketsConnected[nSocketsConnected++] = p;
					nSocketsDynamic++;

					int v = 1;
					setsockopt(pstrAccept->sock, SOL_SOCKET, 0x04, &v, sizeof(v));
				}
				else if (pstrAccept->sock >= 0) {
					close(pstrAccept->sock);
				}
			}
			else {
				pSocket->_close();
			}
		}
		break;

		case SOCKET_MSG_CONNECT: {
			tstrSocketConnectMsg* pstrConnect = (tstrSocketConnectMsg*) pvMsg;

			if (pstrConnect && pstrConnect->s8Error >= 0) {
				if (pSocket->cb_read != nullptr) {
					recv(sock, pTCPBuffer, MAIN_WIFI_M2M_BUFFER_SIZE, 0);
				}
				pSocket->u8State = Socket::CONNECTED;
			}
			else {
				pSocket->u8State = Socket::IDLE;
			}
		}
		break;

		case SOCKET_MSG_SEND: {
			if (pSocket->oPending.bPending) {
				pSocket->pending();
			}
			else if (pSocket->cb_send) {
				pSocket->cb_send(*pSocket);
			}
		}
		break;

		case SOCKET_MSG_RECV: {
			tstrSocketRecvMsg* pstrRecv = (tstrSocketRecvMsg*) pvMsg;

			if (pstrRecv && pstrRecv->s16BufferSize > 0) {
				if (pSocket->cb_read != nullptr) {
					recv(sock, pTCPBuffer, MAIN_WIFI_M2M_BUFFER_SIZE, 0);
					pstrRecv->pu8Buffer[pstrRecv->s16BufferSize] = '\0';
					pSocket->cb_read(*pSocket, *pstrRecv);
				}
			}
			else {
				pSocket->_close();
			}
		}
		break;

		case SOCKET_MSG_SENDTO: {
			if (pSocket->oPending.bPending) {
				pSocket->pending();
			}
			else {
				if (pSocket->cb_read) {
					recvfrom(sock, pUDPBuffer, MAIN_WIFI_M2M_BUFFER_SIZE, 0);
				}
				if (pSocket->cb_send) {
					pSocket->cb_send(*pSocket);
				}
			}
		}
		break;

		case SOCKET_MSG_RECVFROM: {
			tstrSocketRecvMsg* pstrRx = (tstrSocketRecvMsg*) pvMsg;

			if (pstrRx->pu8Buffer && pstrRx->s16BufferSize) {
				if (pSocket->cb_read != nullptr) {
					recvfrom(sock, pUDPBuffer, MAIN_WIFI_M2M_BUFFER_SIZE, 0);
					pstrRx->pu8Buffer[pstrRx->s16BufferSize] = '\0';
					pSocket->cb_read(*pSocket, *pstrRx);
				}
			}
			else if (pstrRx->s16BufferSize == SOCK_ERR_TIMEOUT) {
				/* Prepare next buffer reception. */
				recvfrom(sock, pUDPBuffer, MAIN_WIFI_M2M_BUFFER_SIZE, 0);
			}
			else {
				pSocket->_close();
			}
		}
		break;

		default:
		break;
	}
}
