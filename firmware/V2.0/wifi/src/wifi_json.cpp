////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "inc/wifi.hpp"
extern "C" {
#include "json.h"
}

enum cmd {
	CMD_POWER = 1,
	CMD_DCF77,
	CMD_NIGHT,
	CMD_LDR,
	CMD_FTIME,

	CMD_MODE,
	CMD_TIMEZONE,
	CMD_B_MIN,
	CMD_B_MAX,
	CMD_COLOR,
	CMD_ANIMATION,
	CMD_LANGUAGE,
	CMD_OFFHOURS,
	CMD_OFFMINUTES,

	CMD_SWITCH = 100
};

struct jString{
	char * sStr;
	uint16_t u16Pos;
};

static void strcat(jString& sJson, char* sStr){
	while(*sStr != '\0'){
		sJson.sStr[sJson.u16Pos] = *sStr;
		sJson.u16Pos++;
		sStr++;
	}

	sJson.sStr[sJson.u16Pos] = '\0';
}

static void charcat(jString& sJson, char sStr){
	sJson.sStr[sJson.u16Pos] = sStr;
	sJson.u16Pos++;
	sJson.sStr[sJson.u16Pos] = '\0';
}

static void addBool(jString& sJson, char* sName, bool bVal) {
	charcat(sJson, '\"');
	strcat(sJson, sName);
	strcat(sJson, (char*)"\":");

	if (bVal) {
		strcat(sJson, (char*) "true,");
	}
	else {
		strcat(sJson, (char*) "false,");
	}
}

static void addInt(jString& sJson, char* sName, int bVal) {
	charcat(sJson, '\"');
	strcat(sJson, sName);
	strcat(sJson, (char*)"\":");

	char buf[10];
	itoa(bVal, buf, 10);
	strcat(sJson, buf);
	charcat(sJson, ',');
}

static void addString(jString& sJson, char* sName, char* sVal) {
	charcat(sJson, '\"');
	strcat(sJson, sName);
	strcat(sJson, (char*)"\":\"");
	strcat(sJson, sVal);
	strcat(sJson, (char*)"\",");
}

void Wifi::createJson() {
	json[0] = '\0';
	jString oJson;
	oJson.sStr = json;
	oJson.u16Pos = 0;

	charcat(oJson, '{');
	addBool(oJson, (char*) "bOn", oWordclock.bOn);
	addBool(oJson, (char*) "bDCF77", oWordclock.oSettings.oData.bDCF77);
	addBool(oJson, (char*) "bNight", oWordclock.oSettings.oData.bNight);
	addBool(oJson, (char*) "bEnableLDR", oWordclock.oSettings.oData.bEnableLDR);
	addBool(oJson, (char*) "bFetchInternetTime", oWordclock.oSettings.oData.bFetchInternetTime);

	addInt(oJson, (char*) "eMode", oWordclock.oSettings.oData.eMode);
	addInt(oJson, (char*) "s8TimeZone", oWordclock.oSettings.oData.s8TimeZone);
	addInt(oJson, (char*) "u8BrightnessMin", oWordclock.oSettings.oData.u8BrightnessMin);
	addInt(oJson, (char*) "u8BrightnessMax", oWordclock.oSettings.oData.u8BrightnessMax);
	addInt(oJson, (char*) "u8Red", oWordclock.oSettings.oData.oColors.getRed());
	addInt(oJson, (char*) "u8Green", oWordclock.oSettings.oData.oColors.getGreen());
	addInt(oJson, (char*) "u8Blue", oWordclock.oSettings.oData.oColors.getBlue());
	addInt(oJson, (char*) "u8White", oWordclock.oSettings.oData.oColors.getWhite());
	addInt(oJson, (char*) "eAnimation", oWordclock.oSettings.oData.eAnimation);
	addInt(oJson, (char*) "oLanguage", oWordclock.oSettings.oData.oLanguage);
	addInt(oJson, (char*) "rtcNightOff.u8Hours", oWordclock.oSettings.oData.rtcNightOff.u8Hours);
	addInt(oJson, (char*) "rtcNightOff.u8Minutes", oWordclock.oSettings.oData.rtcNightOff.u8Minutes);
	addInt(oJson, (char*) "rtcNightOn.u8Hours", oWordclock.oSettings.oData.rtcNightOn.u8Hours);
	addInt(oJson, (char*) "rtcNightOn.u8Minutes", oWordclock.oSettings.oData.rtcNightOn.u8Minutes);

	addString(oJson, (char*) "ssid", oWordclock.oSettings.oData.ssid);
	addString(oJson, (char*) "wpakey", oWordclock.oSettings.oData.wpakey);
	addString(oJson, (char*) "deviceName", oWordclock.oSettings.oData.deviceName);
	addString(oJson, (char*) "timeserver", oWordclock.oSettings.oData.timeserver);

	addInt(oJson, (char*) "u8Display", (uint8_t) oWordclock.CurrentMenu);
	strcat(oJson, (char*)"\"dummy\":0}");
}

void Wifi::processJson(char* sJson, bool bSaveinstant) {
	bool bSave = true;

	jsonParser oJson;
	jsonObj root = json_parser_init(&oJson, (char*) sJson);

	int iCmd = json_object_get_int(&oJson, root, (char*) "cmd");
	bool bVal;
	int iVal;

	switch (iCmd) {
		case CMD_POWER:
			oWordclock.bOn = oWordclock.bOn ? false : true;
		break;

		case CMD_DCF77:
			bVal = json_object_get_boolean(&oJson, root, (char*) "val");
			oWordclock.oSettings.oData.bDCF77 = bVal;
		break;

		case CMD_NIGHT:
			bVal = json_object_get_boolean(&oJson, root, (char*) "val");
			oWordclock.oSettings.oData.bNight = bVal;
		break;

		case CMD_LDR:
			bVal = json_object_get_boolean(&oJson, root, (char*) "val");
			oWordclock.oSettings.oData.bEnableLDR = bVal;
		break;

		case CMD_FTIME:
			bVal = json_object_get_boolean(&oJson, root, (char*) "val");
			oWordclock.oSettings.oData.bFetchInternetTime = bVal;
		break;

		case CMD_MODE:
			iVal = json_object_get_int(&oJson, root, (char*) "val");
			oWordclock.oSettings.oData.eMode = (DisplayMode) iVal;
		break;

		case CMD_TIMEZONE:
			iVal = json_object_get_int(&oJson, root, (char*) "val");
			oWordclock.oSettings.oData.s8TimeZone = iVal;
		break;

		case CMD_COLOR: {
			int iObj = json_get_object(&oJson, root, (char*) "val");
			iVal = json_object_get_int(&oJson, iObj, (char*) "red");
			oWordclock.oSettings.oData.oColors.setRed(iVal);
			iVal = json_object_get_int(&oJson, iObj, (char*) "green");
			oWordclock.oSettings.oData.oColors.setGreen(iVal);
			iVal = json_object_get_int(&oJson, iObj, (char*) "blue");
			oWordclock.oSettings.oData.oColors.setBlue(iVal);
			iVal = json_object_get_int(&oJson, iObj, (char*) "white");
			oWordclock.oSettings.oData.oColors.setWhite(iVal);
		}
		break;

		case CMD_SWITCH: {
			Menu eVal = (Menu) json_object_get_int(&oJson, root, (char*) "val");

			switch (eVal) {
				case Menu::Clock:
					oWordclock.setMenu(Menu::Clock);
				break;

#ifdef ENABLE_SECONDS
				case Menu::Seconds:
					oWordclock.setMenu(Menu::Seconds);
				break;
#endif

#ifdef ENABLE_TEMP
				case Menu::Temp:
					oWordclock.setMenu(Menu::Temp);
				break;
#endif

				default:
				break;
			}
		}
		break;

		default:
			bSave = false;
		break;
	}

	if (bSave && bSaveinstant) {
		oWordclock.oSettings.writeChanges();
	}
}
