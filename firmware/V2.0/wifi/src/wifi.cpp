////////////////////////////////////////////////////////////////////////////////
// Wordclock V2.0
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "inc/wifi.hpp"
#include "inc/htmlfiles.hpp"

bool Wifi::bForceReconnect = false;
Wifi::Status Wifi::u8ConnectionStatus = Wifi::Status::DISCONNECTED;
Timer Wifi::tTimeoutConnection;
Timer Wifi::tRestart;
Socket Wifi::udpBroadcast;
Socket Wifi::HTTPServer;
Socket Wifi::AppServer;
char Wifi::json[1000];

void Wifi::init() {
	int8_t ret;
	tstrWifiInitParam param;

	bForceReconnect = false;
	u8ConnectionStatus = Status::DISCONNECTED;

	// Initialize the BSP.
	nm_bsp_deinit();
	nm_bsp_reset();

	/* Initialize Wi-Fi parameters structure. */
	memset((uint8_t*) &param, 0, sizeof(tstrWifiInitParam));

	/* Initialize Wi-Fi driver with data and status callbacks. */
	param.pfAppWifiCb = Wifi::wifi_cb;
	ret = m2m_wifi_init(&param);
	if (M2M_SUCCESS == ret) {
		setHostname();
		m2m_wifi_configure_sntp((uint8_t*) oWordclock.oSettings.oData.timeserver, strlen(oWordclock.oSettings.oData.timeserver), SNTP_ENABLE_DHCP);

		/*sockets */
		socketInit();
		registerSocketCallback(Socket::socket_cb, NULL);
	}
}

void Wifi::loop() {
	m2m_wifi_handle_events(NULL);

	switch (u8ConnectionStatus) {
		case Status::DISCONNECTED:
			if (tTimeoutConnection.Ready()) {
				setHostname();
				m2m_wifi_configure_sntp((uint8_t*) oWordclock.oSettings.oData.timeserver, strlen(oWordclock.oSettings.oData.timeserver), SNTP_ENABLE_DHCP);

				if (oWordclock.oSettings.oData.bAccessPoint || *oWordclock.oSettings.oData.ssid == '\0' || *oWordclock.oSettings.oData.wpakey == '\0') {
					createAP((char*) oWordclock.oSettings.oData.ssid, nullptr);
				}
				else {
					connectToWifi((char*) oWordclock.oSettings.oData.ssid, (char*) oWordclock.oSettings.oData.wpakey);
				}
			}
		break;

		case Status::ACCESSPOINT_INIT:
			if (tRestart.Ready()) {
				u8ConnectionStatus = Status::ACCESSPOINT;
			}
		break;

		case Status::CONNECTED_INIT:
			u8ConnectionStatus = Status::CONNECTED;
		break;

		case Status::CONNECTED:
		case Status::ACCESSPOINT:
			open_sockets();

#ifndef ENABLE_WIFI_NIGHTMODE
			if (oWordclock.isNight()) {
				close_connection();
				break;
			}
#endif

			if (u8ConnectionStatus == Status::ACCESSPOINT && !oWordclock.oSettings.oData.bAccessPoint && tTimeoutConnection.Ready()) {
				bForceReconnect = true;
			}

			if (bForceReconnect && Socket::nConnections() == 0) {
				bForceReconnect = false;
				close_connection();
			}
		break;

		default:
		break;
	}
}

void Wifi::open_sockets() {
#ifdef ENABLE_APPSUPPORT
	udpBroadcast._open(PORT_UDP_BROADCAST, 0, Socket::UDP, [](Socket& socket, tstrSocketRecvMsg& pstrRx) {
		if ((size_t) pstrRx.s16BufferSize == strlen(UDP_REQUEST) && strcmp(pstrRx.pu8Buffer, UDP_REQUEST) == 0) {
			struct sockaddr_in addr;
			addr.sin_family = AF_INET;
			addr.sin_port = _htons(PORT_UDP_BROADCAST);
			addr.sin_addr.s_addr = pstrRx.strRemoteAddr.sin_addr.s_addr;

			const char* sResp = UDP_RESPONSE;
			socket._send((char*) sResp, (struct sockaddr*) &addr);
		}
		else if ((size_t) pstrRx.s16BufferSize == strlen(UDP_IDENTIFY) && strcmp(pstrRx.pu8Buffer, UDP_IDENTIFY) == 0) {
			oWordclock.identify();
		}
	});

	AppServer._open(PORT_TCP_APPSERVER, 0, Socket::TCP_SERVER, [](Socket& socket, tstrSocketRecvMsg& pstrRx) {
		processJson((char*) pstrRx.pu8Buffer, false);
		oWordclock.forceRedraw();
		createJson();
		socket._send(json, 0);
	},[]() {
		oWordclock.oSettings.writeChanges();
	});
#endif

#ifdef ENABLE_WEBVISU
	HTTPServer._open(80, 0, Socket::TCP_SERVER, [](Socket& socket, tstrSocketRecvMsg& pstrRx) {
		HTTPParser oParser((const char*) pstrRx.pu8Buffer);
		char sHeaderResponse[100];
		bool bClose = false;

		char s404[] = "HTTP/1.1 404\r\n\r\nObject Not Found";
		uint8_t* sResp = (uint8_t*) s404;
		uint32_t u32Length = strlen(s404);

		while (oParser.nextHeader()) {
			if (strcmp((uint8_t*) oParser.getHeader(), "Connection") == 0) {
				if (strcmp((uint8_t*) oParser.getHeaderValue(), "close") == 0) {
					bClose = true;
				}
			}
		}

		if (Socket::ConnectionLimit()) {
			char s429[] = "HTTP/1.1 429\r\n\r\n";
			sResp = (uint8_t*) s429;
			u32Length = strlen(s429);
			bClose = true;
		}
		else if (oParser.isRessource("/data")) {
			if (oParser.isBody()) {
				processJson((char*) oParser.getBody());
				oWordclock.forceRedraw();
			}

			createJson();
			sResp = (uint8_t*) json;
			u32Length = strlen(json);

			strcpy(sHeaderResponse, "HTTP/1.0 200 OK");
			strcat(sHeaderResponse, "\r\nCache-Control: no-cache");
			strcat(sHeaderResponse, "\r\nContent-Type: application/json");
			strcat(sHeaderResponse, "\r\nContent-Length: ");
			itoa(u32Length, &sHeaderResponse[strlen(sHeaderResponse)], 10);
			strcat(sHeaderResponse, "\r\n\r\n");

			socket._send(sHeaderResponse, 0);
		}
		else {
			for (uint8_t i = 0; i < nFiles; i++) {
				if (oParser.isRessource(arrFiles[i]->name)) {
					sResp = (uint8_t*) arrFiles[i]->b;
					u32Length = arrFiles[i]->u32Size;

					strcpy(sHeaderResponse, "HTTP/1.0 200 OK");
					strcat(sHeaderResponse, "\r\nCache-Control: no-cache");
					strcat(sHeaderResponse, "\r\nContent-Type: ");
					strcat(sHeaderResponse, arrFiles[i]->contentType);
					strcat(sHeaderResponse, "\r\nContent-Length: ");
					itoa(arrFiles[i]->u32Size, &sHeaderResponse[strlen(sHeaderResponse)], 10);
					strcat(sHeaderResponse, "\r\n\r\n");

					socket._send(sHeaderResponse, 0);

					break;
				}
			}
		}

		if (bClose) {
			socket._send(sResp, u32Length, 0, [](Socket& socket) {
				socket._close();
			});
		}
		else {
			socket._send(sResp, u32Length, 0);
		}
	}
	);

#endif
}
